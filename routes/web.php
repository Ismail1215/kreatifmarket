<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/dynamic_dependent', 'DynamicDependentController@index1');

Route::post('dynamic_dependent/fetch', 'DynamicDependentController@fetch')->name('dynamicdependent.fetch');




Auth::routes();

Route::get('register', 'AuthenticationController@disablereg')->name('register');
Route::get('login', 'AuthenticationController@disablelog')->name('login');

Route::get('password/reset', 'AuthenticationController@disableforgot');
//Route::get('login', 'AuthenticationController@disablelog')->name('login');

Route::get('registers', 'AuthenticationController@ShowRegisterForm')->name('custom.register');
Route::post('registers', 'AuthenticationController@register');


Route::get('logins', 'AuthenticationController@ShowLoginForm')->name('custom.login');
Route::post('logins', 'AuthenticationController@login');


Route::get('forgot-password', 'AuthenticationController@ShowForgotForm')->name('custom.forgot_password');
Route::post('forgot-password', 'AuthenticationController@forgot')->name('lupalapulak');



Route::get('/plan_price_packages', 'VisitorViewController@planpricepack')->name('planpricepack');
Route::get('/', 'VisitorViewController@homepage')->name('homepage');



Route::get('/home', 'HomeController@index')->name('home');
 



Route::group(['prefix'=>'account'],function(){

//=====================Users===========================


	Route::get('/overview', function(){
		return view('pages.User.user_profile');
	})->name('overview');


	Route::get('/profile', function(){
		return view('pages.User.user_profile');
	})->name('profile');


	Route::get('/likes', function(){
		return view('pages.user_like');
	})->name('likes');


	Route::get('/collections', function(){
		return view('pages.user_profile');
	})->name('collections');


	Route::get('/save_designs', function(){
		return view('pages.user_profile');
	})->name('save_designs');


	Route::get('/downloads', function(){
		return view('pages.user_profile');
	})->name('downloads');


	Route::get('/plan&pricing', function(){
		return view('pages.user_profile');
	})->name('plan&pricing');


	Route::get('/billings', function(){
		return view('pages.user_profile');
	})->name('billings');


	Route::get('/purchase', function(){
		return view('pages.user_profile');
	})->name('purchase');



	Route::get('/helpdesk', function(){
		return view('pages.user_profile');
	})->name('helpdesk');



	



});


//insert path into mysql==================================insert_project

//Route::post('/insert','Controller@insert');





//update path from mysql==================================manager/update_agent/{{ $data->id }}
Route::get('manager/update_Contract/{id}','UpdateController@update_Contract_True');
Route::post('manager/update_Contract/manager/update_contract2','UpdateController@update_Contract2');


//delete path from mysql==================================
Route::get('manager/manager/delete_Contract/{id}','DeleteController@deleteContractTrue');


//Route::get('manager/manager/delete_team/{id}','DeleteController@deleteTeamTrue');



//select query to view to page===============================

//Route::get('account/overview','UserViewController@View_Overview')->name('overview');

Route::get('account/profile','UserViewController@View_Profile')->name('profile');
Route::get('account/profile-edit','UserEditController@ViewEditProfile')->name('profile-edit');
Route::post('account/profile-edit02','UserEditController@EditProfileTrue')->name('profile-edit02');
Route::post('account/profile-edit02b','UserEditController@EditProfileTrue2')->name('profile-edit02b');


Route::get('account/likes','UserViewController@View_Like')->name('likes');

Route::get('account/collections','UserViewController@View_Collection')->name('collections');

Route::get('account/save_designs','UserViewController@View_Save_Design')->name('save_designs');

Route::get('account/downloads','UserViewController@View_Download')->name('downloads');

Route::get('account/plan&pricing','UserViewController@View_Plan_Pricing')->name('plan&pricing');

Route::get('account/billings','UserViewController@View_Billing')->name('billings');
Route::get('account/billings-edit','UserEditController@ViewEditBillings')->name('billings-edit');
Route::post('account/billing-edit-addr','UserEditController@EditbillingTrue1')->name('billing-edit-addr');

Route::get('account/purchase','UserViewController@View_Purchase')->name('purchase');


Route::get('account/become-contributor','UserViewController@View_become_contrib')->name('become-contributor');
Route::post('account/update-be-contributor','UserEditController@EditbecontribTrue')->name('update-be-contributor');



Route::get('account/helpdesk','UserViewController@View_Helpdesk')->name('helpdesk');

//Route::get('account/logout','UserViewController@View_Logout')->name('logout');







Route::group(['prefix'=>'contributor'],function(){

//=====================Users===========================


	Route::get('/overview', function(){
		return view('pages.User.user_profile');
	})->name('overview');


	Route::get('/profile', function(){
		return view('pages.User.user_profile');
	})->name('profile');


	Route::get('/likes', function(){
		return view('pages.user_like');
	})->name('likes');


	Route::get('/collections', function(){
		return view('pages.user_profile');
	})->name('collections');


	Route::get('/save_designs', function(){
		return view('pages.user_profile');
	})->name('save_designs');


	Route::get('/downloads', function(){
		return view('pages.user_profile');
	})->name('downloads');


	Route::get('/plan&pricing', function(){
		return view('pages.user_profile');
	})->name('plan&pricing');


	Route::get('/billings', function(){
		return view('pages.user_profile');
	})->name('billings');


	Route::get('/purchase', function(){
		return view('pages.user_profile');
	})->name('purchase');



	Route::get('/helpdesk', function(){
		return view('pages.user_profile');
	})->name('helpdesk');






});



Route::get('contributor/overview','ContributorViewController@View_Overview')->name('overview-contributor');

Route::get('contributor/profile','ContributorViewController@View_Profile')->name('profile-contributor');
Route::get('contributor/profile-edit','ContributorEditController@ViewEditProfile')->name('profile-contrib-edit');
Route::post('contributor/profile-edit-about','ContributorEditController@EditProfileTrue1')->name('profile-edit-about');
Route::post('contributor/profile-edit-pwd','ContributorEditController@EditProfileTrue2')->name('profile-edit-pwd');
Route::post('contributor/profile-edit-addr','ContributorEditController@EditProfileTrue3')->name('profile-edit-addr');
Route::post('contributor/profile-edit-kreatifmarket-url','ContributorEditController@EditProfileTrue4')->name('profile-edit-kreatifmarket-url');
Route::post('contributor/profile-edit-contact','ContributorEditController@EditProfileTrue5')->name('profile-edit-contact');
Route::post('contributor/profile-edit-bank','ContributorEditController@EditProfileTrue6')->name('profile-edit-bank');
//Route::get('contributor/profile-edit2','UserEditController@ViewEditProfile2')->name('profile-edit2');


Route::get('contributor/likes','ContributorViewController@View_Like')->name('likes-contributor');

Route::get('contributor/collections','ContributorViewController@View_Collection')->name('collections-contributor');

Route::get('contributor/save_designs','ContributorViewController@View_Save_Design')->name('save_designs-contributor');

Route::get('contributor/downloads','ContributorViewController@View_Download')->name('downloads-contributor');

Route::get('contributor/plan&pricing','ContributorViewController@View_Plan_Pricing')->name('plan&pricing-contributor');

Route::get('contributor/billings','ContributorViewController@View_Billing')->name('billings-contributor');
Route::get('contributor/billings-edit','ContributorEditController@ViewEditBillings')->name('billings-contributor-edit');
Route::post('contributor/billing-contrib-edit-addr','ContributorEditController@editbillingtrue')->name('billing-contrib-edit-addr');

Route::get('contributor/purchase','ContributorViewController@View_Purchase')->name('purchase-contributor');




Route::get('contributor/upload_content','ContributorViewController@View_UploadContent')->name('upload_content-contributor');

Route::get('contributor/upload_content_image','ContributorViewController@View_UploadContent_Image')->name('upload_content_img-contributor');

Route::get('contributor/upload_content_image_02','ContributorViewController@View_UploadContent_Image02')->name('upload_content-img02-contributor');

Route::post('contributor/upload_content_send_img','ContributorViewController@View_UploadContent_Send_Image')->name('upload-content-send-img');

Route::get('contributor/upload_content_audio','ContributorViewController@View_UploadContent_Audio')->name('upload_content_audio-contributor');

Route::get('contributor/upload_content_vedio','ContributorViewController@View_UploadContent_Vedio')->name('upload_content_video-contributor');





Route::get('contributor/content_gallery','ContributorViewController@View_ContentGallery')->name('content_gallery-contributor');

Route::get('contributor/content_status','ContributorViewController@View_ContentStatus')->name('content_status-contributor');

Route::get('contributor/earnings','ContributorViewController@View_Earning')->name('earnings-contributor');

Route::get('contributor/helpdesk','ContributorViewController@View_Helpdesk')->name('helpdesk-contributor');

//Route::get('contributor/logout','ContributorViewController@View_Logout')->name('logout-contributor');






Route::group(['prefix'=>'evaluator'],function(){

//=====================Users===========================


	Route::get('/overview', function(){
		return view('pages.User.user_profile');
	})->name('overview');


	Route::get('/profile', function(){
		return view('pages.User.user_profile');
	})->name('profile');


	Route::get('/likes', function(){
		return view('pages.user_like');
	})->name('likes');


	Route::get('/collections', function(){
		return view('pages.user_profile');
	})->name('collections');


	Route::get('/save_designs', function(){
		return view('pages.user_profile');
	})->name('save_designs');


	Route::get('/downloads', function(){
		return view('pages.user_profile');
	})->name('downloads');


	Route::get('/plan&pricing', function(){
		return view('pages.user_profile');
	})->name('plan&pricing');


	Route::get('/billings', function(){
		return view('pages.user_profile');
	})->name('billings');


	Route::get('/purchase', function(){
		return view('pages.user_profile');
	})->name('purchase');



	Route::get('/helpdesk', function(){
		return view('pages.user_profile');
	})->name('helpdesk');






});



Route::get('evaluator/overview','EvaluatorViewController@View_Overview')->name('overview-evaluator');

Route::get('evaluator/profile','EvaluatorViewController@View_Profile')->name('profile-evaluator');
Route::get('evaluator/profile-edit','EvaluatorEditController@ViewEditProfile')->name('profile-evaluator-edit');
//Route::get('evaluator/profile-edit2','UserEditController@ViewEditProfile2')->name('profile-edit2');


Route::get('evaluator/likes','EvaluatorViewController@View_Like')->name('likes-evaluator');

Route::get('evaluator/collections','EvaluatorViewController@View_Collection')->name('collections-evaluator');

Route::get('evaluator/save_designs','EvaluatorViewController@View_Save_Design')->name('save_designs-evaluator');

Route::get('evaluator/downloads','EvaluatorViewController@View_Download')->name('downloads-evaluator');

Route::get('evaluator/plan&pricing','EvaluatorViewController@View_Plan_Pricing')->name('plan&pricing-evaluator');

Route::get('evaluator/billings','EvaluatorViewController@View_Billing')->name('billings-evaluator');
//Route::get('evaluator/billings-edit','UserEditController@ViewEditBillings')->name('billings-edit');
//Route::get('evaluator/billing-contrib-edit-addr','UserEditController@ViewEditBillings2')->name('billing-contrib-edit-addr');

Route::get('evaluator/purchase','EvaluatorViewController@View_Purchase')->name('purchase-evaluator');

Route::get('evaluator/upload_content','EvaluatorViewController@View_UploadContent')->name('upload_content-evaluator');

Route::get('evaluator/content_gallery','EvaluatorViewController@View_ContentGallery')->name('content_gallery-evaluator');

Route::get('evaluator/content_status','EvaluatorViewController@View_ContentStatus')->name('content_status-evaluator');

Route::get('evaluator/earnings','EvaluatorViewController@View_Earning')->name('earnings-evaluator');

Route::get('evaluator/helpdesk','EvaluatorViewController@View_Helpdesk')->name('helpdesk-evaluator');

//Route::get('evaluator/logout','evaluatorViewController@View_Logout')->name('logout-evaluator');












Route::group(['prefix'=>'admin'],function(){

//=====================Users===========================


	Route::get('/overview', function(){
		return view('pages.User.user_profile');
	})->name('overview');


	Route::get('/profile', function(){
		return view('pages.User.user_profile');
	})->name('profile');


	Route::get('/likes', function(){
		return view('pages.user_like');
	})->name('likes');


	Route::get('/collections', function(){
		return view('pages.user_profile');
	})->name('collections');


	Route::get('/save_designs', function(){
		return view('pages.user_profile');
	})->name('save_designs');


	Route::get('/downloads', function(){
		return view('pages.user_profile');
	})->name('downloads');


	Route::get('/plan&pricing', function(){
		return view('pages.user_profile');
	})->name('plan&pricing');


	Route::get('/billings', function(){
		return view('pages.user_profile');
	})->name('billings');


	Route::get('/purchase', function(){
		return view('pages.user_profile');
	})->name('purchase');



	Route::get('/helpdesk', function(){
		return view('pages.user_profile');
	})->name('helpdesk');






});



Route::get('admin/overview','SU_AdminViewController@View_Overview')->name('overview-admin');

Route::get('admin/profile','SU_AdminViewController@View_Profile')->name('profile-admin');
//Route::get('admin/profile-edit','adminEditController@ViewEditProfile')->name('profile-admin-edit');
//Route::get('admin/profile-edit2','UserEditController@ViewEditProfile2')->name('profile-edit2');


Route::get('admin/likes','SU_AdminViewController@View_Like')->name('likes-admin');

Route::get('admin/collections','SU_AdminViewController@View_Collection')->name('collections-admin');

Route::get('admin/save_designs','SU_AdminViewController@View_Save_Design')->name('save_designs-admin');

Route::get('admin/downloads','SU_AdminViewController@View_Download')->name('downloads-admin');

Route::get('admin/plan&pricing','SU_AdminViewController@View_Plan_Pricing')->name('plan&pricing-admin');

Route::get('admin/billings','SU_AdminViewController@View_Billing')->name('billings-admin');
//Route::get('admin/billings-edit','UserEditController@ViewEditBillings')->name('billings-edit');
//Route::get('admin/billing-contrib-edit-addr','UserEditController@ViewEditBillings2')->name('billing-contrib-edit-addr');

Route::get('admin/purchase','SU_AdminViewController@View_Purchase')->name('purchase-admin');

Route::get('admin/upload_content','SU_AdminViewController@View_UploadContent')->name('upload_content-admin');

Route::get('admin/content_gallery','SU_AdminViewController@View_ContentGallery')->name('content_gallery-admin');

Route::get('admin/content_status','SU_AdminViewController@View_ContentStatus')->name('content_status-admin');

Route::get('admin/earnings','SU_AdminViewController@View_Earning')->name('earnings-admin');

Route::get('admin/helpdesk','SU_AdminViewController@View_Helpdesk')->name('helpdesk-admin');

//Route::get('admin/logout','adminViewController@View_Logout')->name('logout-admin');