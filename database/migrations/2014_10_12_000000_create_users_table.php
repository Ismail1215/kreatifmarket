<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name','30');
            $table->string('last_name','30')->nullable();
            $table->string('username','30')->nullable();
            $table->string('email')->unique();
           // $table->timestamp('email_verified_at');
            $table->string('password');
            $table->integer('level')->default('4');
            $table->char('ic')->nullable();
            $table->char('phone','14')->nullable();
            $table->string('img_prf')->nullable();
            $table->string('img_prf_size')->nullable();
            $table->longtext('desc_contrib')->nullable();
            $table->string('interest','100')->nullable();
            $table->string('company','100')->nullable();
            $table->string('line1','100')->nullable();
            $table->string('line2','100')->nullable();
            $table->string('line3','100')->nullable();
            $table->string('state','40')->nullable();
            $table->string('country','30')->nullable();
            $table->char('postcode','10')->nullable();
            $table->string('city','20')->nullable();
            $table->string('bank_name','30')->nullable();
            $table->string('bank_acc','30')->nullable();
            $table->string('status_acc','10')->default('1');
            $table->integer('status_prt')->default('0');
            $table->string('url_contrib','100')->nullable();
            $table->string('ig_contrib','100')->nullable();
            $table->string('gmail_contrib','100')->nullable();
            $table->string('fb_contrib','100')->nullable();
            $table->string('linked_contrib','100')->nullable();
            $table->string('url_website','100')->nullable();
            $table->string('url_portfolio','100')->nullable();
            $table->string('twitter_contrib','100')->nullable();
            $table->string('fb_link','100')->nullable();
            $table->string('gmail_link','100')->nullable();
            $table->string('payment_method','100')->nullable();
            $table->string('card_num','30')->nullable();
            $table->string('card_name','100')->nullable();
            $table->string('card_date','30')->nullable();
            $table->string('card_cvv','30')->nullable();
            $table->string('set_as','30')->nullable();
            



            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
