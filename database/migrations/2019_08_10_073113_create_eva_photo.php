<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEvaPhoto extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('eva_photo', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('photo_name','100')->nullable();
            $table->string('photo_path','255')->nullable();
            $table->string('photo_size','50')->nullable();
            $table->integer('data_arkib_in_1')->default('0');
            $table->integer('data_arkib_in_2')->default('0');
            
            $table->integer('user_id')->nullable();
            $table->string('content_type','20')->nullable();
            $table->string('photo_cat','30')->nullable();
            $table->string('editorial_use','20')->nullable();
            $table->string('content_name','30')->nullable();
            $table->longtext('desc')->nullable();
            $table->string('outright_price','20')->nullable();
            $table->integer('tag_id')->nullable();
            $table->string('keyword_service','30')->nullable();
            $table->integer('form_id')->nullable();          

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('eva_photo');
    }
}
