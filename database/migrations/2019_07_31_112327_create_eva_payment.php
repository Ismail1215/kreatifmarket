<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEvaPayment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('eva_payment', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('payment_method','30')->nullable();
            $table->binary('type_img')->nullable();
            $table->integer('flag')->default('1');
          

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('eva_payment');
    }
}
