<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEvaBank extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('eva_bank', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('bank_name','20');
            //$table->string('bank_acc','14');
            //$table->char('color','15');
            $table->integer('flag')->deafult('1'); 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('eva_bank');
    }
}
