<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class eva_users extends Model
{
    protected $table = 'users';

	protected $fillable = [
        'name','last_name','email','phone','ic','interest','desc_contrib','line1','line2','line3','company','city','state','postcode','country','phone','bank_name','bank_acc', 'created_at', 'updated_at',
    ];
}
