<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Illuminate\Foundation\Validation\ValidatesRequests;
use validate;
use Illuminate\Support\Facades\Hash;

class UserEditController extends Controller
{


    public function ViewEditProfile(){


      $id_user = auth()->user()->id;

      $user_details = \App\customer_profile::get_all_user()->where('id', $id_user);     

      //dd($user_details);
      
      return view ('pages.Account.user_profile_edit',['user_details'=>$user_details]);
       

    }


    public function EditProfileTrue(Request $request){


      $id_user = auth()->user()->id;
	//echo $id_user;

      $this->validate_profile($request);
    
   		DB::table('users')->where('id', $id_user)->update([

    	'name' => $request->input('name'),
    	'last_name' => $request->input('last_name'),
    	'email' => $request->input('email'),
    	'phone' => $request->input('phone'),
    	'ic' => $request->input('ic'),
    	'img_prf' => $request->input('img_prf'),
    	
    ]);     

     // $name = $request->input('name');
      //$last_name = $request->input('last_name');


   //   echo $name;
      //echo $last_name;

     // dd(request()->all());

     // \App\eva_users::create($request->all());

      return redirect('account/profile');
     
      //return redirect()->back()->with('error');
       

    }


    public function validate_profile($request){

      return $this->validate($request, [
         
         'email'=>'email|', 
         'phone'=>'required|numeric', 
         'ic'=>'required|numeric|min:12', 



      ]);


    }




    public function EditProfileTrue2(Request $request){


      $id_user = auth()->user()->id;
  //echo $id_user;

      $this->validate_profile2($request);

       //$request['now_password'] = bcrypt($request->now_password);

     //  $test = Hash::make($request->now_password);

       //dd($test);

    
      DB::table('users')->where('id', $id_user)->update([

      'password' => Hash::make($request->password),
      
      
    ]);
      

     // $name = $request->input('name');
      //$last_name = $request->input('last_name');


   //   echo $name;
      //echo $last_name;

     // dd(request()->all());

     // \App\eva_users::create($request->all());


      return redirect('account/profile');
     
     // return redirect()->back()->with('error');
       

    }


    public function validate_profile2($request){

      return $this->validate($request, [
         
     

        

        'now_password'          => 'required|min:8',
        'password'              => 'required|min:8|confirmed|different:now_password',
        'password_confirmation' => 'required|min:8',



      ]);


    }

    




    public function EditbecontribTrue(Request $request){

	$id_user = auth()->user()->id;

  $this->validate_bcmcontrib($request);

	//echo $id_user;
  //dd($request);
    
    DB::table('users')->where('id', $id_user)->update([

    	'name' => $request->input('name'),
    	'last_name' => $request->input('last_name'),
      'username' => $request->input('username'),
    	'email' => $request->input('email'),
    	'phone' => $request->input('phone'),
    	'ic' => $request->input('ic'),
    	'interest' => $request->input('interest'),
    	'desc_contrib' => $request->input('desc_contrib'),
    	'img_prf' => $request->input('img_prf'),
    	'company' => $request->input('company'),
    	'line1' => $request->input('line1'),
    	'line2' => $request->input('line2'),
    	'line3' => $request->input('line3'),
    	'country' => $request->input('country'),
    	'state' => $request->input('state'),
    	'city' => $request->input('city'),
    	'postcode' => $request->input('postcode'),
    	'bank_name' => $request->input('bankname'),
    	'bank_acc' => $request->input('bank_acc'),
    	'level' => '2',
    ]);

     
      return redirect('contributor/profile');

    }

    public function validate_bcmcontrib($request){

      return $this->validate($request, [
         'ic'=>'required|numeric', 
         'phone'=>'required|numeric',
         'username'=>'unique:users',  
         'postcode'=>'required|numeric',  
         'bank_acc'=>'required|numeric', 
      ]);


    }


    public function ViewEditBillings(){


      $id_user = auth()->user()->id;

      $user_details = \App\customer_profile::get_all_user()->where('id', $id_user);     

      //dd($user_details);
      
      return view ('pages.Account.billing-edit',['user_details'=>$user_details]);

       //return view ('pages.Account.billing-edit',['user_details'=>$user_details]);

    }



    public function EditbillingTrue1(Request $request){

      $id_user = auth()->user()->id;

  //echo $id_user;

      $this->validate_bill_addr($request);

     // dd($request);
    
      DB::table('users')->where('id', $id_user)->update([

      
      'company' => $request->input('company'),
      'line1' => $request->input('line1'),
      'line2' => $request->input('line2'),
      'line3' => $request->input('line3'),
      'country' => $request->input('country'),
      'state' => $request->input('state'),
      'city' => $request->input('city'),
      'postcode' => $request->input('postcode'),
   
     
    ]);

     
      return redirect('account/billings');

    }

     public function validate_bill_addr($request){

      return $this->validate($request, [
         
         'postcode'=>'numeric',

         ]);
    }




}
