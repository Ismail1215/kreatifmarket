<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SU_AdminViewController extends Controller
{
    



     public static function View_Overview(Request $request){

     // $id_user = auth()->user()->id;

     // $user_details = \App\admin_profile::get_all_user()->where('id', $id_user);     

      //dd($user_details);
      
      return view ('pages.superadmin.overview-admin');

 

    }


    public static function View_Profile(Request $request){

      $id_user = auth()->user()->id;

      $user_details = \App\admin_profile::get_all_user()->where('id', $id_user);     

      //dd($user_details);
      
      return view ('pages.superadmin.profile-admin',['user_details'=>$user_details]);

 

    }



    
}
