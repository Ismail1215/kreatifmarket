<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class UserInsertController extends Controller
{
    




public function insert_contrib(Request $request){

	$id_user = auth()->user()->id;

	//echo $id_user;
    
    DB::table('users')->where('id', $id_user)->update([

    	'name' => $request->input('name'),
    	'last_name' => $request->input('last_name'),
    	'email' => $request->input('email'),
    	'phone' => $request->input('phone'),
    	'ic' => $request->input('ic'),
    	'interest' => $request->input('interest'),
    	'desc_contrib' => $request->input('desc_contrib'),
    	'img_prf' => $request->input('img_prf'),
    	'company' => $request->input('company'),
    	'line1' => $request->input('line1'),
    	'line2' => $request->input('line2'),
    	'line3' => $request->input('line3'),
    	'country' => $request->input('country'),
    	'state' => $request->input('state'),
    	'city' => $request->input('city'),
    	'postcode' => $request->input('postcode'),
    	'bank_name' => $request->input('bank_name'),
    	'bank_acc' => $request->input('bank_acc'),
    ]);

      //$this->validate_contrib($request);

     // $name = $request->input('name');
      //$last_name = $request->input('last_name');


   //   echo $name;
      //echo $last_name;

     // dd(request()->all());

     // \App\eva_users::create($request->all());
     
     // return redirect()->back();

    }

    public function validate_contrib($request){

      return $this->validate($request, [
         'ic'=>'required|unique:users',   
      ]);


    }







}
