<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class UserViewController extends Controller
{
    
public function __construct()
{
    $this->middleware('auth');

}

    public static function View_Overview(){

      $id_user = auth()->user()->id;

      $user_details = \App\customer_profile::get_all_user()->where('id', $id_user);    
      //return view('pages.assign_role_01',['roleassign'=>$roleassign,'role'=>$role]);  
      return view ('pages.Account.overview',['user_details'=>$user_details]);

    }



    public static function View_Profile(Request $request){

      $id_user = auth()->user()->id;

      $user_details = \App\customer_profile::get_all_user()->where('id', $id_user);

           

      //dd($user_details);
      
      return view ('pages.Account.user_profile',['user_details'=>$user_details]);

 

    }






    public static function View_Like(){

      return view ('pages.Account.like');

    }



    public static function View_Collection(){

      return view ('pages.Account.collection');

    }




    public static function View_Save_Design(){

      return view ('pages.Account.save_design');

    }




    public static function View_Download(){

      return view ('pages.Account.download');

    }




    public static function View_Plan_Pricing(){

      return view ('pages.Account.plan_pricing');

    }




    public static function View_Billing(){

      $id_user = auth()->user()->id;

      $user_details = \App\customer_profile::get_all_user()->where('id', $id_user);     

      //dd($user_details);
      
      return view ('pages.Account.billing',['user_details'=>$user_details]);



     // return view ('pages.Account.billing');

    }




    public static function View_Purchase(){

      return view ('pages.Account.purchase');

    }


    public static function View_become_contrib(){


      $id_user = auth()->user()->id;

      $user_details = \App\customer_profile::get_all_user()->where('id', $id_user);     

      //dd($user_details);
      
      

      $bank_name = \App\eva_bank::get_all_bank();


      //dd($bank_name);

      return view ('pages.Account.become-contributor',['bank_name'=>$bank_name,'user_details'=>$user_details]);

    

    }




    public static function View_Helpdesk(){

      return view ('pages.Account.overview');

    }




    public static function View_Logout(){

      return view ('pages.Account.overview');

    }








}
