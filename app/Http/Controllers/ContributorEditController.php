<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Hash;

class ContributorEditController extends Controller
{
    
   


    public function ViewEditProfile(){


      $id_user = auth()->user()->id;

      $user_details = \App\customer_profile::get_all_user()->where('id', $id_user); 

      $bank_name = \App\eva_bank::get_all_bank();    

      //dd($user_details);
      
      return view ('pages.contributor.profile-contrib-edit',['bank_name'=>$bank_name,'user_details'=>$user_details]);
      //return view ('pages.contributor.profile-contrib-edit');

    }


    public function EditProfileTrue1(request $request){


       $id_user = auth()->user()->id;
	//echo $id_user;

       $pic = $request->file('profile_photo');
       $picName = $pic->getClientOriginalName();
       $folder = "product/profile_img/";

       $pic->move($folder,$picName);

       $picUrl = $folder.$picName;


       // dd($request);

        
        $this->validate_profile_about($request);

        //dd($request);
    
   		DB::table('users')->where('id', $id_user)->update([

    	'name' => $request->input('name'),
    	'last_name' => $request->input('last_name'),
      'username' => $request->input('username'),
    	'email' => $request->input('email'),
    	'phone' => $request->input('phone'),
    	'ic' => $request->input('ic'),
    	'img_prf' => $picUrl,
    	'interest' => $request->input('interest'),
    	'desc_contrib' => $request->input('desc_contrib'),    	
    	
    	
    ]);

      
      return redirect('contributor/profile');
       

    }


     public function validate_profile_about($request){

      return $this->validate($request, [
         
         'email'=>'email|', 
         'phone'=>'required|numeric', 
         'ic'=>'required|numeric|min:12', 



      ]);


    }



    public function EditProfileTrue2(Request $request){


        $id_user = auth()->user()->id;
  //echo $id_user;
        $this->validate_profile_pwd($request);

       // dd($request);

      DB::table('users')->where('id', $id_user)->update([

      'password' => Hash::make($request->password),
      
      
    ]); 
       
      
  

      
      return redirect('contributor/profile');
       

    }


     public function validate_profile_pwd($request){

      return $this->validate($request, [
         
        'old_password'          => 'required|min:8',
        'password'              => 'required|min:8|confirmed|different:old_password',
        'password_confirmation' => 'required|min:8', 



      ]);


    }





    public function EditProfileTrue3(Request $request){


      	$id_user = auth()->user()->id;
	//echo $id_user;

        //dd($request);
    
   		DB::table('users')->where('id', $id_user)->update([

    	
    	'company' => $request->input('company'),
    	'line1' => $request->input('line1'),
    	'line2' => $request->input('line2'),
    	'line3' => $request->input('line3'),
    	'country' => $request->input('country'),
    	'state' => $request->input('state'),
    	'city' => $request->input('city'),
    	'postcode' => $request->input('postcode'),
    	
    ]);

      //$this->validate_contrib($request);

     // $name = $request->input('name');
      //$last_name = $request->input('last_name');


   //   echo $name;
      //echo $last_name;

     // dd(request()->all());

     // \App\eva_users::create($request->all());
     
      return redirect('contributor/profile');
       

    }



    public function EditProfileTrue4(Request $request){


        $id_user = auth()->user()->id;
  //echo $id_user;

       // dd($request);
    
      DB::table('users')->where('id', $id_user)->update([

      
      'url_portfolio' => $request->input('url_portfolio'),
      'status_prt' => '1',

      
    ]);

      //$this->validate_contrib($request);

     // $name = $request->input('name');
      //$last_name = $request->input('last_name');


   //   echo $name;
      //echo $last_name;

     // dd(request()->all());

     // \App\eva_users::create($request->all());
     
      return redirect('contributor/profile');
       

    }






    public function EditProfileTrue5(Request $request){


        $id_user = auth()->user()->id;
  //echo $id_user;

      //  dd($request);

       
    
      DB::table('users')->where('id', $id_user)->update([

      
      'url_website' =>$request->input('website'),
      'twitter_contrib' => $request->input('twitter'),
      'ig_contrib' => $request->input('insta'),
      'fb_contrib' => $request->input('fb'),
      'gmail_contrib' => $request->input('google'),
      'linked_contrib' => $request->input('linked'),
      
    ]);

    
     
      return redirect('contributor/profile');
       

    }


    



    public function EditProfileTrue6(Request $request){


        $id_user = auth()->user()->id;
  //echo $id_user;

       // dd($id_user);

        $this->bankcontrib($request);
    
      DB::table('users')->where('id', $id_user)->update([

      
      'bank_name' => $request->input('bankname'),
      'bank_acc' => $request->input('bank_acc'),
      
    ]);

    
     
      return redirect('contributor/profile');
       

    }


     public function bankcontrib($request){

      return $this->validate($request, [
          
         'bank_acc'=>'required|numeric', 
      ]);


    }




    public function ViewEditBillings(){


      $id_user = auth()->user()->id;

      $user_details = \App\customer_profile::get_all_user()->where('id', $id_user);     

      //dd($user_details);
      //echo "masuk";
      
      return view ('pages.contributor.billing-contrib-edit',['user_details'=>$user_details]);

       //return view ('pages.Account.billing-edit',['user_details'=>$user_details]);

    }


    public function editbillingtrue(Request $request){

      $id_user = auth()->user()->id;

  //echo $id_user;
      //dd($id_user);

      $this->validate_bill_addr_contrib($request);

     // dd($request);
    
      DB::table('users')->where('id', $id_user)->update([

      
      'company' => $request->input('company'),
      'line1' => $request->input('line1'),
      'line2' => $request->input('line2'),
      'line3' => $request->input('line3'),
      'country' => $request->input('country'),
      'state' => $request->input('state'),
      'city' => $request->input('city'),
      'postcode' => $request->input('postcode'),
   
     
    ]);

     
      return redirect('contributor/billings');

    }

     public function validate_bill_addr_contrib($request){

      return $this->validate($request, [
         
         'postcode'=>'numeric',

         ]);
    }



}
