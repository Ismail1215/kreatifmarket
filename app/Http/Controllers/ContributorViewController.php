<?php

namespace App\Http\Controllers;

use App\eva_photo;

use Illuminate\Http\Request;
use DB;
class ContributorViewController extends Controller
{
   

    public function __construct()
{
    $this->middleware('auth');

}


    public static function View_Overview(){

      return view ('pages.contributor.overview-contrib');

    }



    public static function View_Profile(){

      $id_user = auth()->user()->id;

      $user_details = \App\contributor_profile::get_all_user()->where('id', $id_user);     

      //dd($user_details);
      
      return view ('pages.contributor.profile-contrib',['user_details'=>$user_details]);

      //return view ('pages.contributor.profile-contrib');

    }



    public static function View_Like(){

      return view ('pages.contributor.like-contrib');

    }



    public static function View_Collection(){

      return view ('pages.contributor.collection-contrib');

    }


    public static function View_Save_Design(){

      return view ('pages.contributor.saved_design-contrib');

    }



    public static function View_Download(){

      return view ('pages.contributor.download_history-contrib');

    }



    public static function View_Plan_Pricing(){

      return view ('pages.contributor.plan_price-contrib');


    }



    public static function View_Billing(){

       $id_user = auth()->user()->id;

      $user_details = \App\contributor_profile::get_all_user()->where('id', $id_user);     

      //dd($user_details);
      
      return view ('pages.contributor.billing-contrib',['user_details'=>$user_details]);


      //return view ('pages.contributor.billing-contrib');

    }



    public static function View_Purchase(){

      return view ('pages.contributor.purchase_history-contrib');

    }


  




    public static function View_UploadContent(){

      $id_user = auth()->user()->id;

      $user_details = \App\contributor_profile::get_all_user()->where('id', $id_user);     

      //dd($user_details);
      
      return view ('pages.contributor.upload_content-contrib',['user_details'=>$user_details]);

     

    }


    public static function View_UploadContent_Image(){

      $id_user = auth()->user()->id;

      $user_details = \App\contributor_profile::get_all_user()->where('id', $id_user);     

      //dd($user_details);
      
      return view ('pages.contributor.upload_content-img-contrib',['user_details'=>$user_details]);

    

    }


    public static function View_UploadContent_Send_Image(Request $request){


      $id_user = auth()->user()->id;


    /*  $this->validate($request, [

                'filenames' => 'required',

                'filenames.*' => 'mimes:doc,pdf,docx,zip'

        ]);
*/

          // dd($request);
      if($request->hasfile('filename'))
         {

            foreach($request->file('filename') as $image)
            {

              $eva_photo= new eva_photo();

                $name=$image->getClientOriginalName();
                $size = $image->getClientSize();

                $folder = "product/contributor/";
                $image->move($folder,$name);

                $picUrl = $folder.$name;
               // $image->move(public_path().'/images/', $name);  
                $data = $name; 
                $data1 = $picUrl; 
                $data2 = $size;
               // $data_arkib = $id_user; 

                $eva_photo->photo_name= $data;
                $eva_photo->photo_path= $data1;
                $eva_photo->photo_size= $data2;
                $eva_photo->user_id= $id_user;

                $eva_photo->save();
            }
         }

      return redirect('contributor/upload_content_image_02');
     // return redirect('contributor/profile');
        
 

    }



    public static function View_UploadContent_Image02(Request $request){

      $id_user = auth()->user()->id;

      $photos = \App\eva_photo::get_all_photo()->where('user_id', $id_user)->where('data_arkib_in_1','0');    



      dd($photos);





     //return view('pages.contributor.upload_content-img02-contrib');       
 

    }



    public static function View_UploadContent_Audio(){

      $id_user = auth()->user()->id;

      $user_details = \App\contributor_profile::get_all_user()->where('id', $id_user);     

      //dd($user_details);
      
      return view ('pages.contributor.upload_content-audio-contrib',['user_details'=>$user_details]);

     
    }


    public static function View_UploadContent_Vedio(){

      $id_user = auth()->user()->id;

      $user_details = \App\contributor_profile::get_all_user()->where('id', $id_user);     

      //dd($user_details);
      
      return view ('pages.contributor.upload_content-vedio-contrib',['user_details'=>$user_details]);

      

    }


    public static function View_UploadContent_Vedio2(){

      $id_user = auth()->user()->id;

      $user_details = \App\contributor_profile::get_all_user()->where('id', $id_user);     

      //dd($user_details);
      
      return view ('pages.contributor.upload_content-vedio-contrib',['user_details'=>$user_details]);

      

    }



    public static function View_ContentGallery(){

      return view ('pages.contributor.content_galery-contrib');

    }



    public static function View_ContentStatus(){

      return view ('pages.contributor.content_status-contrib');

    }


    public static function View_Earning(){

      return view ('pages.contributor.earning-contrib');

    }



    public static function View_Helpdesk(){

      return view ('pages.contributor.helpdesk-contrib');

    }




    public static function View_Logout(){

      return view ('pages.contributor.logout-contrib');

    }




    
}
