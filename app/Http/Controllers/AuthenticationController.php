<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use Auth;
use validate;
use validation;
//use Illuminate\Support\Facades\Auth;

class AuthenticationController extends Controller
{






    public function disableforgot(Request $request)
    {
      abort(404);
    }

    
    public function disablereg(Request $request)
    {
      abort(404);
    }

    public function disablelog(Request $request)
    {
      abort(404);
    }



     public function ShowForgotForm(){

      return view ('customs.forgot_password');

    }

     public function forgot(Request $request){


      //echo "masuk";

     //$this->validation($request);

     //$request['password'] = bcrypt($request->password);
     //user::create($request->all());


     //return redirect ('/account/profile')->with('Status','you are registed');
     //return redirect ('/account/profile');

      //return $request->all();

    }




	 public function ShowRegisterForm(){

      return view ('customs.registor');

    }

     public function register(Request $request){

     $this->validation($request);

     $request['password'] = bcrypt($request->password);
     user::create($request->all());


     //return redirect ('/account/profile')->with('Status','you are registed');
     return redirect ('/account/profile');

      //return $request->all();

    } 

     public function validation($request){

      return $this->validate($request, [
        'name' => 'required|max:30|min:',
        'last_name' => 'required|max:30|min:',
        'email' => 'required|email|unique:users|max:30',

        'password' => 'required|confirmed|max:30|min:8',
        //'terms' => 'required' // This is check checkbox is checked |regex:/[a-z]/|regex:/[A-Z]/|regex:/[0-9]/|regex:/[@$!%*#?&]/

      ]);


    }


     public function ShowLoginForm(){

      return view ('customs.login');

    }



     public function login(Request $request){

      $this->validate($request, [
        
        'email' => 'required|email|max:255',
        'password' => 'required|max:255',

      ]);




      if (Auth::attempt(['email'=>$request->email, 'password'=>$request->password])){      	

     //  user 4 contrib 2      
           $level_usr = auth()->user()->level;
           echo $level_usr;

           if ($level_usr==4){

            //echo "user";
            return redirect ('/account/profile');
           }

           elseif ($level_usr==2) {

            //echo "contrib";
            return redirect ('/contributor/profile');            
        
           }

           elseif ($level_usr==3) {

            //echo "evaluator";
            return redirect ('/evaluator/profile');
            
           }

           elseif ($level_usr==1) {

            //echo "admin";
            return redirect ('/admin/profile');
            
           }
      	//return redirect ('/contributor/profile')->with('Status','you are registed');
      	//$this->except('logout');

        //return redirect ('/account/profile');
        //return redirect ('/contributor/profile');

      }
     

     else{
     	//echo "woopss paassword and username doesnt match log in again";
      return redirect ('/logins')->with('danger','password not match');
     }

    }
}
