<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class eva_photo extends Model
{

	protected $table = 'eva_photo';
	
    protected $fillable = [
        'photo_name','photo_path','photo_size',
    ];



    public static function get_all_photo(){
    	

    	  $photo= eva_photo::get();    	 
       	  return $photo;  
       
    }

}
