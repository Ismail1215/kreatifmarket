<!DOCTYPE html>
<html>
<head>

	<title>@yield('title')</title>

	@include('layouts.header-customer')

</head>

<body>

	@include('layouts.navmenu-customer')


	<div class="container shadow-lg">
        <div class="row">
            <div class="col" style="background-color: #ffffff;">

               @yield('contributor-thing')  
                            

            </div>

            <div class="col-xl-3">

                @yield('contributor-thing-menu')
                
            </div>
        </div>
    </div>




    <footer class="shadow-lg footer-mgn">

    	@include('layouts.footer-customer')
        
    </footer>

 
	<script type="text/javascript" src="{{URL::asset('js/jquery.min.js')}}"></script>
    <script type="text/javascript" src="{{URL::asset('js/bootstrap.min.js')}}"></script>
    <script type="text/javascript">$("#profileImage").click(function(e) {
    $("#imageUpload").click();
});

function fasterPreview( uploader ) {
    if ( uploader.files && uploader.files[0] ){
          $('#profileImage').attr('src', 
             window.URL.createObjectURL(uploader.files[0]) );
    }
}

$("#imageUpload").change(function(){
    fasterPreview( this );
});</script>

    


</body>
</html>

