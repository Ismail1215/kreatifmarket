

    <div class="row rowgrid1">
        <div class="col-lg-4 gridset1">
            <div class="row">
                <div class="col-lg-4 gridset1"><img src="{{ url('images/dummy/image 5.1.png') }}" /></div>
            </div>
            <div class="row">
                <div class="col-lg-4 gridset1"><img src="{{ url('images/dummy/image 6.2.png') }}" /></div>
            </div>
            <div class="row">
                <div class="col-lg-4 gridset1"><img src="{{ url('images/dummy/image 5.1.png') }}" /></div>
            </div>
            <div class="row">
                <div class="col-lg-4 gridset1"><img src="{{ url('images/dummy/image 9.png') }}" /></div>
            </div>
        </div>
        <div class="col-lg-4 gridset1">
            <div class="row">
                <div class="col-lg-4 gridset1"><img src="{{ url('images/dummy/image 5.1.png') }}" /></div>
            </div>
            <div class="row">
                <div class="col-lg-4 gridset2"><img src="{{ url('images/dummy/image 5.png') }}" /></div>
            </div>
            <div class="row">
                <div class="col-lg-4 gridset1"><img src="{{ url('images/dummy/image 6.2.png') }}" /></div>
            </div>
        </div>
        <div class="col-lg-4 gridset1">
            <div class="row">
                <div class="col-lg-4 gridset2"><img src="{{ url('images/dummy/image 4.svg') }}" /></div>
            </div>
            <div class="row">
                <div class="col-lg-4 gridset2"><img src="{{ url('images/dummy/image 4.svg') }}" /></div>
            </div>
        </div>
    </div>
    <div class="row rowgrid1">
        <div class="col-lg-4 gridset1">
            <div class="row">
                <div class="col-lg-4 gridset1"><img src="{{ url('images/dummy/image 6.2.png') }}" /></div>
            </div>
            <div class="row">
                <div class="col-lg-4 gridset2"><img src="{{ url('images/dummy/image 8.png') }}" /></div>
            </div>
        </div>
        <div class="col-lg-4 gridset1">
            <div class="row">
                <div class="col-lg-4 gridset2"><img src="{{ url('images/dummy/image 4.svg') }}" /></div>
            </div>
            <div class="row">
                <div class="col-lg-4 gridset1"><img src="{{ url('images/dummy/image 7.png') }}" /></div>
            </div>
        </div>
        <div class="col-lg-4 gridset1">
            <div class="row">
                <div class="col-lg-4 gridset1"><img src="{{ url('images/dummy/image 7.png') }}" /></div>
            </div>
            <div class="row">
                <div class="col-lg-4 gridset2"><img src="{{ url('images/dummy/image 5.png') }}" /></div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col"></div>
        <div class="col">
            <nav>
                <ul class="pagination">
                    <li class="page-item"><a class="page-link" aria-label="Previous"><span aria-hidden="true">«</span></a></li>
                    <li class="page-item"><a class="page-link">1</a></li>
                    <li class="page-item"><a class="page-link">2</a></li>
                    <li class="page-item"><a class="page-link">3</a></li>
                    <li class="page-item"><a class="page-link">4</a></li>
                    <li class="page-item"><a class="page-link">5</a></li>
                    <li class="page-item"><a class="page-link" aria-label="Next"><span aria-hidden="true">»</span></a></li>
                </ul>
            </nav>
        </div>
        <div class="col"></div>
    </div>