<!DOCTYPE html>
<html>

<head>

    <title>@yield('title')</title>

    @include('layouts.header-visitor')

    
</head>

<body>
    @include('layouts.navmenu2a')
    <section>
        <div class="container">
            <div class="row row-space-detail-box">
                <div class="col col-details">
                    @yield('contributor-thing2a')
                </div>
                
                    @yield('contributor-thing-menu2a')
                
            </div>
        </div>
    </section>

    <section id="footer">
        @include('layouts.footer1')        
    </section>

    <section id="footer-merge">
        @include('layouts.footer1-merge')       
    </section>


     <script type="text/javascript" src="{{URL::asset('js/jquery.min.js')}}"></script>
    <script type="text/javascript" src="{{URL::asset('js/bootstrap.min.js')}}"></script>

  
    
    <script type="text/javascript" src="{{URL::asset('js/bs-animation.js')}}"></script>
    <script type="text/javascript" src="{{URL::asset('js/aos.js')}}"></script>

    <script type="text/javascript">$('input[type=file]').on('change', function() {
   
     $.each(this.files, function(i, file) {
     $('#filename').append('<p>' + file.name + '</p>');
    });
    });

    $('input[type=file]').on('dragenter', function() {
    $('div').addClass('dragover');
    });

    $('input[type=file]').on('dragleave', function() {
        $('div').removeClass('dragover');
    });

    </script>

</body>

</html>