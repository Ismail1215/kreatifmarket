<!DOCTYPE html>

<html>

<head>
<title>@yield('title')</title>
@include('layouts.header')
</head>

<body>

<div class="header"> <img src="{{URL::asset('images/examedia.png')}}"></div><!-- <i class='fa fa-bank'></i>  -->     
            
    

<div class="topnav" id="navscroll">
 @include('layouts.mainmenu')
<div class="logout">@include('layouts.logoutmenu')</div>
</div>


<div class="row">
   <div class="leftcolumn">
    <div class="card">@yield('column1')</div>
    <div class="card">@yield('column2')</div>
  </div>


  <div class="rightcolumn">
    <div class="card">@yield('column3')</div>
    <div class="card">@yield('column4')</div>
    <div class="card">@yield('column5')</div>
  </div>
</div>

<!-- End here -->

<!-- start footer-->
<div class="footer">@include('layouts.footer')</div>
<!-- end footer-->

</body>
</html>


