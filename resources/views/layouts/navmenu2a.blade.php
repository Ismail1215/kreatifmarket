<div id="navbar-collaspe" class="navbar sticky-top">
        <div class="container container-nv-collps">
            <div class="collapse-logo"><img src="assets/img/navbar1/Logo.svg"></div>
            <div class="search-collapse"><input type="search"><img src="assets/img/navbar1/Hover%202.png"></div>
            <div class="collapse-buttn"><img src="assets/img/navbar1/Shape.png"></div>
        </div>
    </div>
    <div id="navbar-normal" class="navbar sticky-top">
        <div class="container">
            <div class="menuu-logo"><a href="{{ route('homepage')}}"><img src="{{ url('images/navbar1/Logo.svg') }}" href="#"></a></div>
            <div class="dropdown show"><a class="text-white btn btn-transparent dropdown-toggle" id="dropdownMenuLink" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="font-size: 15px;font-family: 'Roboto';">Images</a>
                <div class="dropdown-menu"
                    aria-labelledby="dropdownMenuLink"><a class="dropdown-item" href="#">Link</a><a class="dropdown-item" href="#">Link</a><a class="dropdown-item" href="#">Link</a><a class="dropdown-item" href="#">Link</a></div>
            </div>
            <div class="menuu-search"><input type="search" placeholder="search"><a href="#"><img src="{{ url('images/navbar1/Hover%202.png') }}"></a></div>
            <div class="dropdown show"><a class="text-white btn btn-transparent dropdown-toggle" id="dropdownMenuLink" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="font-size: 14px;font-family: 'Roboto';">Discover</a>
                <div class="dropdown-menu"
                    aria-labelledby="dropdownMenuLink"><a class="dropdown-item" href="#">Link</a><a class="dropdown-item" href="#">Link</a><a class="dropdown-item" href="#">Link</a><a class="dropdown-item" href="#">Link</a></div>
            </div>
            <div class="menuu">
                <ul>
                    <li><a href="#">Become Contributor</a></li>
                    <li><a href="#">Plans &amp; Pricing</a></li>
                </ul>
            </div>
            <div class="dropdown show"><a class="text-white btn btn-transparent dropdown-toggle" id="dropdownMenuLink" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="font-size: 14px;font-family: 'Roboto';">My Account</a>
                <div class="dropdown-menu"
                    aria-labelledby="dropdownMenuLink"><a class="dropdown-item" href="#">Link</a><a class="dropdown-item" href="#">Link</a><a class="dropdown-item" href="#">Link</a><a class="dropdown-item" href="#">Link</a></div>
            </div>
        </div>
    </div>