<div>
        <nav class="navbar navbar-dark navbar-expand-md sticky-top bg-dark shadow-lg">
            <div class="container"><a class="navbar-brand" href="/"><img src="{{ url('images/Logo.svg') }}"/></a><button class="navbar-toggler" data-toggle="collapse" data-target="#navcol-1"><span class="sr-only">Toggle navigation</span><span class="navbar-toggler-icon"></span></button>
                <div
                    class="collapse navbar-collapse" id="navcol-1">
                    <form class="form-inline flex-grow-1 justify-content-start navbar-left">
                        <div class="input-group">
                            <li class="dropdown drpimge">
                                <a class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false" href="#">Image</a>
                                <div class="dropdown-menu menu-img-sect" role="menu">
                                    <a class="dropdown-item" role="presentation" href="#">Vedio</a><a class="dropdown-item" role="presentation" href="#">Audio</a>
                                    <a class="dropdown-item" role="presentation" href="#">Blog</a>
                                </div>
                            </li><input class="shadow form-control serchbox" type="text" name="input" placeholder="Search" autofocus="" aria-describedby="basic-addon1"><span id="basic-addon1" class="input-group-addon icn-serch" style="margin-top: 7px;margin-left: 5px;"><i class="fa fa-search"></i></span></div>
                    </form>
                    <ul class="nav navbar-nav flex-grow-1 justify-content-end ml-auto">
                        <li class="nav-item dropdown">
                           <a class="dropdown-toggle nav-link" data-toggle="dropdown" aria-expanded="false" href="#">Discover</a>
                            <div class="dropdown-menu" role="menu">
                                <a class="dropdown-item" role="presentation" href="#">Categories</a><a class="dropdown-item" role="presentation" href="#">About</a>
                                <a class="dropdown-item" role="presentation" href="#">Legals</a>

                                <a class="dropdown-item" role="presentation" href="#">MyNIC</a>
                                <a class="dropdown-item" role="presentation" href="#">Support</a>

                            </div>
                        </li>
                        <li class="nav-item" role="presentation"><a class="nav-link" href="#">Plans &amp; Pricings</a></li>
                        <li class="nav-item" role="presentation"><a class="nav-link" href="#">Become Contributor</a></li>

                        <li class="nav-item" role="presentation"><a class="nav-link" href="{{ route('custom.login') }}">Account</a></li>

                        <li class="nav-item" role="presentation"><a class="nav-link" href="{{ route('custom.register') }}">Sign Up</a></li>
                        
                    </ul>
            </div>
    </div>
    </nav>
    </div>