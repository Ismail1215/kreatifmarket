                <div class="row">
                    <div class="col side-menu-prf">
                        <ul>
                            <li><a class="{{ Request::is('contributor/overview') ? 'active' : null}}" href="{{ route('overview-contributor')}}">Overview</a></li>
                            <li><a class="{{ Request::is('contributor/profile') ? 'active' : null}} {{ Request::is('contributor/profile-edit') ? 'active' : null}} " href="{{ route('profile-contributor')}}">My Profile</a></li>
                            <li><a class="{{ Request::is('contributor/likes') ? 'active' : null}}" href="{{ route('likes-contributor')}}">My Likes</a></li>
                            <li><a class="{{ Request::is('contributor/collections') ? 'active' : null}}" href="{{ route('collections-contributor')}}">Collections</a></li>
                            <li><a class="{{ Request::is('contributor/save_designs') ? 'active' : null}}" href="{{ route('save_designs-contributor')}}">Saved Design</a></li>
                            <li><a class="{{ Request::is('contributor/downloads') ? 'active' : null}}" href="{{ route('downloads-contributor')}}">Download History</a></li>
                        </ul>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col side-menu-prf">
                        <ul>
                            <li><a class="{{ Request::is('contributor/plan&pricing') ? 'active' : null}}" href="{{ route('plan&pricing-contributor')}}">Plans &amp; Pricings</a></li>
                            <li><a class="{{ Request::is('contributor/billings') ? 'active' : null}} {{ Request::is('contributor/billings-edit') ? 'active' : null}} " href="{{ route('billings-contributor')}}">Billings</a></li>
                            <li><a class="{{ Request::is('contributor/purchase') ? 'active' : null}}" href="{{ route('purchase-contributor')}}">Purchase History</a></li>
                        </ul>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col side-menu-prf">
                        <ul>
                            <li><a class="{{ Request::is('contributor/upload_content') ? 'active' : null}}" href="{{ route('upload_content-contributor')}}">Upload Content</a></li>
                            <li><a class="{{ Request::is('contributor/content_gallery') ? 'active' : null}}" href="{{ route('content_gallery-contributor')}}">Content Gallery</a></li>
                            <li><a class="{{ Request::is('contributor/content_status') ? 'active' : null}}" href="{{ route('content_status-contributor')}}">Content Status</a></li>
                            <li><a class="{{ Request::is('contributor/earnings') ? 'active' : null}}" href="{{ route('earnings-contributor')}}">Earnings</a></li>
                        </ul>
                    </div>
                </div>
                
                <hr />
                <div class="row">
                    <div class="col side-menu-prf">
                        <ul>
                            <li><a class="{{ Request::is('contributor/helpdesk') ? 'active' : null}}" href="#">Helpdesk</a></li>
                            <li><a class="{{ Request::is('account/logout') ? 'active' : null}}"  href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">Logout</a><form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form></li>
                        </ul>
                    </div>
                </div>  