                <div class="row">
                    <div class="col side-menu-prf">
                        <ul>
                            <li style="font-size: 18px; font-weight: bold;text-decoration: none;">Overview</li>
                            <li><a class="{{ Request::is('account/profile') ? 'active' : null}} {{ Request::is('account/profile-edit') ? 'active' : null}}" href="{{ route('profile')}}">My Profile</a></li>
                            <li><a class="{{ Request::is('account/likes') ? 'active' : null}}" href="{{ route('likes')}}">My Likes</a></li>
                            <li><a class="{{ Request::is('account/collections') ? 'active' : null}}" href="{{ route('collections')}}">Collections</a></li>
                            <li><a class="{{ Request::is('account/save_designs') ? 'active' : null}}" href="{{ route('save_designs')}}">Saved Design</a></li>
                            <li><a class="{{ Request::is('account/downloads') ? 'active' : null}}" href="{{ route('downloads')}}">Download History</a></li>
                        </ul>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col side-menu-prf">
                        <ul>
                            <li><a class="{{ Request::is('account/plan&pricing') ? 'active' : null}}" href="{{ route('plan&pricing')}}">Plans &amp; Pricings</a></li>
                            <li><a class="{{ Request::is('account/billings') ? 'active' : null}} {{ Request::is('account/billings-edit') ? 'active' : null}}" href="{{ route('billings')}}">Billings</a></li>
                            <li><a class="{{ Request::is('account/purchase') ? 'active' : null}}" href="{{ route('purchase')}}">Purchase History</a></li>
                        </ul>
                    </div>
                </div>
                <hr>
                <div class="row">
                <div class="col text-justify bcme-contrib-info">
                       <p><br />Are you an Artist, Photographer, Graphic Designer, Filmmaker or Creative geniuses which have to have an awesome collections of artistic and creative material? Start earning today!<br /></p>
                </div>
                </div>
                <div class="row">
                    <div class="col bcme-contrib-info"><a class="btn btn-primary border-light shadow-lg" onclick="return confirm('Are you ready to be a Contributor?')" role="button" href="{{ route('become-contributor')}}">Become a contributor!</a></div>
                </div>
                <hr />
                <div class="row">
                    <div class="col side-menu-prf">
                        <ul>
                            <li><a class="{{ Request::is('account/helpdesk') ? 'active' : null}}" href="#">Helpdesk</a></li>
                            <li><a class="{{ Request::is('account/logout') ? 'active' : null}}"  href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">Logout</a><form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form></li>
                        </ul>
                    </div>
                </div>  



