<!DOCTYPE html>
<html>
<head>

	<title>@yield('title')</title>

	@include('layouts.header-customer')

</head>

<body>

	@include('layouts.navmenu-customer')


	<div class="container shadow-sm" style="margin-top: 30px;margin-bottom: 50px">
        <div class="row">
            <div class="col-xl-10 offset-xl-1" style="background-color: #ffffff;">


               @yield('become-contributor-thing')  
                            

            </div>

            
        </div>
    </div>




    <footer class="shadow-lg footer-mgn">

    	@include('layouts.footer-customer')
        
    </footer>

 
	<script type="text/javascript" src="{{URL::asset('js/jquery.min.js')}}"></script>
    <script type="text/javascript" src="{{URL::asset('js/bootstrap.min.js')}}"></script>

    


</body>
</html>

