<div class="container">
            <div class="row rowfoot">
                <div class="col-sm-3"><img src="{{ url('images/footer/Group%203.svg') }}"/></div>
                <div class="col-sm-2" style="text-align: left">
                    <h3>kreatifmarket</h3>
                    <ul>
                        <li><a href="#">Plans &amp; Pricing</a></li>
                        <li><a href="#">Become a Contributor</a></li>
                        <li><a href="#">Categories</a></li>
                        <li><a href="#">Press Releases</a></li>
                        <li><a href="#">Articles &amp; Blogs</a></li>
                    </ul>
                </div>
                <div class="col-sm-2" style="text-align: left">
                    <h3>Legals</h3>
                    <ul>
                        <li><a href="#">Terms of Use</a></li>
                        <li><a href="#">Licenses Agreement</a></li>
                        <li><a href="#">Privacy Policy</a></li>
                        <li><a href="#">Release Forms</a></li>
                        <li><a href="#">Refund Policy</a></li>
                    </ul>
                </div>
                <div class="col-sm-2" style="text-align: left" >
                    <h3>Support</h3>
                    <ul>
                        <li><a href="#">Contact Us</a></li>
                        <li><a href="#">FAQS</a></li>
                    </ul>
                    <h3 style="margin: -2px;">Corporate</h3>
                    <ul>
                        <li><a href="#">About Us</a></li>
                        <li><a target="_blank" href="https://www.mynic.my/en/">MYNIC</a></li>
                    </ul>
                </div>
                <div class="col-sm-3" style="text-align: left">
                    <h3 style="margin: 3px;">Hotline</h3>
                    <h6><strong>1300-88-7277</strong><br></h6>
                    <h3 style="margin: 1px;">Emails</h3>
                    <h6><strong>info</strong>@kreatifmarket.my<br></h6>
                    <h3>Follow us on</h3>
                    <div class="row"><div class="col-img-followus">
    
     <a target="_blank"  href="https://www.facebook.com/mynicberhad"> <img src="{{ url('images/footer/facebook.svg') }}"/></a>
    
     <a target="_blank" href="https://twitter.com/mynicberhad?lang=en"> <img src="{{ url('images/footer/twitter.svg') }}"/></a>
    
     <a target="_blank" href="https://www.instagram.com/mynicberhad/?hl=en"> <img src="{{ url('images/footer/instagram.svg') }}"/></a>
    
     <a target="_blank" href="https://www.linkedin.com/company/mynic-berhad"> <img src="{{ url('images/footer/linkedin.svg') }}"/></a>

     <a target="_blank" href="https://www.mynic.my/en/"> <img src="{{ url('images/footer/googlep.svg') }}"/></a>
    
     






</div></div>
                </div>
            </div>
            <div class="row"><div class="col-copyrght">
    <p>© MYNIC 2018. All rights reserved</p>
</div></div>
        </div>