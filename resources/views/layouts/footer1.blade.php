<div class="container footer-contain">
            <div class="row">
                <div class="col footer-head"><a href="{{ route('homepage')}}" ><img  src="{{ url('images/footer/Group%203.svg') }}"></a></div>
                <div class="col footer-head">
                    <h1>kreatimarket</h1>
                    <ul>
                        <li><a href="{{ route('planpricepack')}}">Plans &amp;Pricings</a></li>
                        <li><a href="#">Become a contributor</a></li>
                        <li><a href="#">Categories</a></li>
                        <li><a href="#">Press Releases</a></li>
                        <li><a href="#">Articles &amp; Blogs</a></li>
                    </ul>
                </div>
                <div class="col footer-head">
                    <h1>Legals</h1>
                    <ul>
                        <li><a href="#">Website Term of Use</a></li>
                        <li><a href="#">License Agreement</a></li>
                        <li><a href="#">Privacy Policy</a></li>
                        <li><a href="#">Release Forms</a></li>
                        <li><a href="#">Refund &nbsp;Policy</a></li>
                    </ul>
                </div>
                <div class="col footer-head">
                    <h1>Support</h1>
                    <ul>
                        <li><a href="#">Contact Us</a></li>
                        <li><a href="#">FAQs</a></li>
                    </ul>
                    <h1 style="margin-top: 8px;">Corporate</h1>
                    <ul>
                        <li><a href="#">About Us</a></li>
                        <li><a href="#">MyNIC</a></li>
                    </ul>
                </div>
                <div class="col footer-head">
                    <h1>Hotline</h1>
                    <h2>1300-88-7277</h2>
                    <h1>Email</h1>
                    <h2>info@kreatifmarket.my<br></h2>
                    <h1>Follow us on</h1>
                    <div class="row">
                        <a target="_blank"  href="https://www.facebook.com/mynicberhad"> <img src="{{ url('images/footer/facebook.svg') }}"/></a>
    
     <a target="_blank" href="https://twitter.com/mynicberhad?lang=en"> <img src="{{ url('images/footer/twitter.svg') }}"/></a>
    
     <a target="_blank" href="https://www.instagram.com/mynicberhad/?hl=en"> <img src="{{ url('images/footer/instagram.svg') }}"/></a>
    
     <a target="_blank" href="https://www.linkedin.com/company/mynic-berhad"> <img src="{{ url('images/footer/linkedin.svg') }}"/></a>

     <a target="_blank" href="https://www.mynic.my/en/"> <img src="{{ url('images/footer/googlep.svg') }}"/></a>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col col-copyrigght"><span>© MYNIC 2018. All rights reserved<br></span></div>
            </div>
        </div>