<!DOCTYPE html>
<html>
<head>
<title>@yield('title')</title>
@include('layouts.header')
</head>
<body>

<div class="header">
  <h1>My Website</h1>
  <p>Resize the browser window to see the effect.</p>
</div>

<div class="topnav">
  <a href="#">Link</a>
  <a href="#">Link</a>
  <a href="#">Link</a>
  <a href="#" style="float:right">Link</a>
</div>

<div class="row">
  <div class="leftcolumn">


   <div class="card">@yield('column1')</div>
    <div class="card">@yield('column2')</div>



  </div>


  <div class="rightcolumn">

    <div class="card">@yield('column3')</div>
    <div class="card">@yield('column4')</div>
    <div class="card">@yield('column5')</div>

  </div>



</div>

<div class="footer">
 @include('layouts.footer')
</div>

</body>
</html>
