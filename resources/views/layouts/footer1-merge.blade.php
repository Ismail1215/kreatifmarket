 <div class="container">
            <div class="row">
                <div class="col col-footer-merge-logo"><img src="{{ url('images/footer/Group%203.svg') }}"></div>
            </div>
            <hr>
            <div class="row">
                <div class="col col-footer-merge">
                    <h1>kreatifmarket</h1>
                </div>
                <div class="col col-footer-merge-right"><i class="fa fa-chevron-circle-down" data-toggle="collapse" data-target="#kreatifmarket"></i></div>
            </div>
            <div class="row">
                <div class="col col-footer-merge collapse" id="kreatifmarket">
                    <ul>
                        <li><a href="#">Plan &amp; Pricing</a></li>
                        <li><a href="#">Become a Contributor</a></li>
                        <li><a href="#">Categories</a></li>
                        <li><a href="#">Press Release&nbsp;</a></li>
                        <li><a href="#">Articles &amp; Blogs</a></li>
                    </ul>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col col-footer-merge">
                    <h1>Legals</h1>
                </div>
                <div class="col col-footer-merge-right"><i class="fa fa-chevron-circle-down" data-toggle="collapse" data-target="#legals"></i></div>
            </div>
            <div class="row">
                <div class="col col-footer-merge collapse" id="legals">
                    <ul>
                        <li><a href="#">Website Terms of Use</a></li>
                        <li><a href="#">License Agreement</a></li>
                        <li><a href="#">Privacy Policy</a></li>
                        <li><a href="#">Release Forms</a></li>
                        <li><a href="#">Refunds Policy</a></li>
                    </ul>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col col-footer-merge">
                    <h1>Support</h1>
                </div>
                <div class="col col-footer-merge-right"><i class="fa fa-chevron-circle-down" data-toggle="collapse" data-target="#support"></i></div>
            </div>
            <div class="row">
                <div class="col col-footer-merge collapse" id="support">
                    <ul>
                        <li><a href="#">Contact Us</a></li>
                        <li><a href="#">FAQs</a></li>
                    </ul>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col col-footer-merge">
                    <h1>corporate</h1>
                </div>
                <div class="col col-footer-merge-right"><i class="fa fa-chevron-circle-down" data-toggle="collapse" data-target="#corporate"></i></div>
            </div>
            <div class="row">
                <div class="col col-footer-merge collapse" id="corporate">
                    <ul>
                        <li><a href="#">About Us</a></li>
                        <li><a href="#">MyNIC</a></li>
                    </ul>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col col-footer-merge">
                    <h1>Hotline</h1>
                </div>
            </div>
            <div class="row">
                <div class="col col-footer-merge">
                    <h2>1300-88-7277</h2>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col col-footer-merge">
                    <h1>Email</h1>
                </div>
            </div>
            <div class="row">
                <div class="col col-footer-merge">
                    <h2>Info@kreatifmarket.my</h2>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col col-footer-merge">
                    <h1>Follow us on&nbsp;</h1>
                </div>
            </div>
            <div class="row">
                <div class="col col-footer-merge-follow"><img src="{{ url('images/footer/facebook.svg') }}"><img src="{{ url('images/footer/twitter.svg') }}"><img src="{{ url('images/footer/instagram.svg') }}"><img src="{{ url('images/footer/linkedin.svg') }}"><img src="{{ url('images/footer/google.svg') }}"></div>
            </div>
            
            <hr>
            <div class="row">
                <div class="col col-footer-merge-copyright"><span>© MYNIC 2018. All rights reserved<br></span></div>
            </div>
        </div>