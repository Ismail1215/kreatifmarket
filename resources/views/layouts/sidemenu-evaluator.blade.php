                <div class="row">
                    <div class="col side-menu-prf">
                        <ul>
                            <li><a class="{{ Request::is('evaluator/overview') ? 'active' : null}}" href="#">Overview</a></li>
                            <li><a class="{{ Request::is('evaluator/profile') ? 'active' : null}} {{ Request::is('evaluator/profile-edit') ? 'active' : null}} " href="#">Evaluator Profile</a></li>
                            <li><a class="{{ Request::is('evaluator/likes') ? 'active' : null}}" href="#">Commission</a></li>
                            <li><a class="{{ Request::is('evaluator/collections') ? 'active' : null}}" href="#">Earnings</a></li>
                            
                        </ul>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col side-menu-prf">
                        <ul>
                            <li><a class="{{ Request::is('evaluator/plan&pricing') ? 'active' : null}}" href="#">Pending Evaluation</a></li>
                            <li><a class="{{ Request::is('evaluator/billings') ? 'active' : null}} " href="#">Evaluation History</a></li>
                           
                        </ul>
                    </div>
                </div>
              
                
                <hr />
                <div class="row">
                    <div class="col side-menu-prf">
                        <ul>
                            <li><a class="{{ Request::is('evaluator/helpdesk') ? 'active' : null}}" href="#">Helpdesk</a></li>
                            <li><a class="{{ Request::is('account/logout') ? 'active' : null}}"  href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">Logout</a><form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form></li>
                        </ul>
                    </div>
                </div> 