<!DOCTYPE html>
<html>

<head>
    
    <title>Login</title>
    @include('layouts.header-customer')
</head>

<body>

    @include('layouts.navmenu-login')

    
    <section id="hero">
        <div class="container">
            <div class="row row-lgn-mgn"></div>
            <div class="row">
                <div class="col-xl-7"></div>
                <div class="col">
                    
                    <div class="row">
                        <div class="col title-register">
                             <form method="POST" action="{{ route('custom.login') }}">
                             @csrf
                            <h2 class="text-center">Sign in to start downloading!</h2>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col reg-input"><input class="shadow-lg @error('email') is-invalid @enderror" type="text" name="email" value="{{ old('email') }}" required="" placeholder="Email*" autofocus="" autocomplete="on" inputmode="email"><small >{{ $errors->first('email') }}</small></div>
                        
                    </div>

                    <div class="row">
                        <div class="col reg-input"><input class="shadow-lg @error('password') is-invalid @enderror" type="password" name="password" required="" placeholder="password*" autofocus="">

                            <small >{{ $errors->first('password') }}</small>
                             @if( Session::has( 'danger' ))
                            <small>{{ Session::get( 'danger' ) }}</small>
                            @endif
                        </div>
                       
                    </div>
                     
                    <div class="row req-field">
                        <div class="col">
                            <div class="info-req">
                                <a style="text-decoration: none;" href="{{ route('custom.forgot_password') }}">
                                    <p>Forgot username/password</p>
                                </a>
                            </div>
                        </div>
                        <div class="col col-btn-reg"><button class="btn btn-primary" type="submit" style="background-color: rgb(168,5,93);border-style: none">Sign In</button></div>
                    </div>
                    </form>
                    <div class="row rowor">
                        <div class="col col-orlines">
                            <div class="orline"><span>or</span></div>
                        </div>
                        <div class="col col-orlines">
                            <div class="orline"></div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col"><div class="jrbtn2"><button  type="button">Connect with Facebook</button></div></div>
                    </div>
                    <div class="row">
                        <div class="col"><div class="jrbtn3"><button  type="button">Connect with Google</button></div></div>
                    </div>
                    <div class="row">
                        <div class="col col-orlines">
                            <div class="orline"></div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col col-policy">
                            <div class="agree-policy"><span>Dont have an account yet?&nbsp;</span><a href="{{ route('custom.register') }}">Sign Up</a><span>&nbsp; today!</span></div>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </section>
    <footer class="footer-mgn">
       @include('layouts.footer-customer')
    </footer>
    <script type="text/javascript" src="{{URL::asset('js/jquery.min.js')}}"></script>
    <script type="text/javascript" src="{{URL::asset('js/bootstrap.min.js')}}"></script>
</body>

</html>