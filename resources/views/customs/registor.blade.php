<!DOCTYPE html>
<html>

<head>
    <title>Registor</title>
    @include('layouts.header-customer')
</head>

<body>

    @include('layouts.navmenu-login')


   
    <section id="hero-reg" style="background-size: cover;">
         <div class="row" style="margin-bottom: 80px"></div>
        <div class="container">
            <div class="row">
                <div class="col-xl-7"></div>
                <div class="col">
                    <div class="row">

                        <form method="POST" action="{{ route('custom.register') }}">
                         {{csrf_field()}}

                        <div class="col title-register">
                            <h1>Don't have account yet? Register now!</h1>
                        </div>
                    </div>
                    <div class="row">


                        <div class="col reg-input"><input class="shadow-lg" type="text" name="name" required="" placeholder="First name*" value="{{ old('name') }}" autofocus="" autocomplete="on">

                            <small >{{ $errors->first('name') }}</small>
                             @if( Session::has( 'danger' ))
                            <small>{{ Session::get( 'danger' ) }}</small>
                            @endif

                        </div>
                    </div>
                    <div class="row">
                        <div class="col reg-input"><input class="shadow-lg" type="text" name="last_name" value="{{ old('last_name') }}" required="" placeholder="Last name*" autofocus="" autocomplete="on">

                            <small >{{ $errors->first('last_name') }}</small>
                             @if( Session::has( 'danger' ))
                            <small>{{ Session::get( 'danger' ) }}</small>
                            @endif

                        </div>
                    </div>
                    <div class="row">
                        <div class="col reg-input"><input class="shadow-lg" type="text" name="email" value="{{ old('email') }}" required="" placeholder="Email*" autofocus="" autocomplete="on" inputmode="email">

                            <small >{{ $errors->first('email') }}</small>
                             @if( Session::has( 'danger' ))
                            <small>{{ Session::get( 'danger' ) }}</small>
                            @endif


                        </div>
                    </div>
                    <div class="row">
                        <div class="col reg-input"><input class="shadow-lg" type="password" name="password" required="" placeholder="password*" autofocus="">


                            <small >{{ $errors->first('password') }}</small>
                             @if( Session::has( 'danger' ))
                            <small>{{ Session::get( 'danger' ) }}</small>
                            @endif


                        </div>
                    </div>
                    <div class="row">
                        <div class="col reg-input"><input class="shadow-lg" type="password" name="password_confirmation" required="" placeholder="Retype-password*" autofocus="">

                            <small >{{ $errors->first('password_confirmation') }}</small>
                             @if( Session::has( 'danger' ))
                            <small>{{ Session::get( 'danger' ) }}</small>
                            @endif


                        </div>
                    </div>
                    <div class="row req-field">
                        <div class="col">
                            <div class="info-req">
                                <p>*Required Fields</p>
                               
                            </div>
                        </div>
                        <div class="col col-btn-reg"><button class="btn btn-primary" style="border-style: none" type="submit">Create</button></div>
                    </div>

                    <div class="row rowor">
                        <div class="col col-orlines">
                            <div class="orline"><span>or</span></div>
                        </div>
                        <div class="col col-orlines">
                            <div class="orline"></div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col"><div class="jrbtn2"><button  type="button">Connect with Facebook</button></div></div>
                    </div>
                    <div class="row">
                        <div class="col"><div class="jrbtn3"><button  type="button">Connect with Google</button></div></div>
                    </div>
                    <div class="row">
                        <div class="col col-policy">
                            <div class="agree-policy"><span>By signing up, you agree to Kreatifmarket's&nbsp;</span><a style="text-decoration: none;" href="#">Terms of Use&nbsp;</a><span>and&nbsp;</span><a style="text-decoration: none;" href="#">Privacy Policy</a></div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col col-orlines">
                            <div class="orline"></div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col col-policy">
                            <div class="agree-policy"><span>Already have an account?&nbsp;</span><a style="text-decoration: none;" href="{{ route('custom.login') }}">Sign in</a></div>
                        </div>
                    </div>

                </div>
                </form>
                
            </div>

        </div>

    </section>
    <footer class="footer-mgn">
         @include('layouts.footer-customer')
    </footer>
    <script type="text/javascript" src="{{URL::asset('js/jquery.min.js')}}"></script>
    <script type="text/javascript" src="{{URL::asset('js/bootstrap.min.js')}}"></script>
</body>

</html>