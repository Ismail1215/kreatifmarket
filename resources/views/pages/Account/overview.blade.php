@extends('layouts.layout01')

@section('title', 'Overview')


@section('customer-thing')
<div class="row">
                    <div class="col text-white prf-title">
                        <h3>Overview</h3>
                    </div>
                </div>
               

@endsection



@section('customer-thing-menu')

                <div class="row">
                    <div class="col text-center prf-side-photo"><img class="border rounded-circle shadow-sm" src="{{ url('images/avatar.png') }}"></div>
                </div>
                <div class="row">
                    <div class="col side-menu-prf">
                        <ul>
                            <li>Hi,<strong> {{ Auth::user()->name }}</strong></li>
                        </ul>
                    </div>
                </div>
                <hr>

                @include('layouts.sidemenu-customer') 
@endsection