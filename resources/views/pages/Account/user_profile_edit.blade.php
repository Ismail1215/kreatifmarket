@extends('layouts.layout01')

@section('title', 'User Profile')


@section('customer-thing')
            <div class="row">
                    <div class="col text-white prf-title">
                        <h3>My Profile</h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col text-black-50 tiitle-info">
                        <h3>Personal Information</h3>
                    </div>
                    
                </div> 
              <form action="profile-edit02" method="post"  >
                {{csrf_field()}}
                <div class="row">

                    @foreach ($user_details as $data) 
                    <div class="col input-prf">
                        <div class="row">
                            <div class="col input-prf-col"><label>First Name</label><input  value="{{ $data->name }}" type="text" name="name"  autofocus=""><small >{{ $errors->first('name') }}</small></div>
                        </div>
                        <div class="row">
                            <div class="col input-prf-col"><label>Last Name</label><input  value=" {{ $data->last_name }}" type="text" name="last_name"  autofocus=""><small >{{ $errors->first('last_name') }}</small></div>
                        </div>
                        <div class="row">
                            <div class="col input-prf-col"><label>Email</label><input  value=" {{ $data->email }}" type="text" name="email"  autofocus=""><small >{{ $errors->first('email') }}</small></div>
                        </div>
                        <div class="row">
                            <div class="col input-prf-col"><label>Contact No.</label><input  value=" {{ $data->phone }}" type="text" name="phone"  autofocus=""><small >{{ $errors->first('phone') }}</small></div>
                        </div>
                        <div class="row">
                            <div class="col input-prf-col"><label>ID/IC/MyKAD No.</label><input  value=" {{ $data->ic }}" type="text" name="ic"  autofocus=""><small >{{ $errors->first('ic') }}</small></div>
                            </div>
                    </div>
                    <div class="col">
                        <div class="row">
                            <div class="col prf-photo">
                                <label style="float: left">Profile Photo</label>

                                 <img  src="{{ url('images/avatar.png') }}">
                                
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
                <div class="row">
    				<div class="col"></div>
    				<div class="col text-right sve-btn-prf"><button class="btn btn-primary border-light shadow" type="button" onclick="window.location.href = '{{ route('profile')}}'" style="background-color: white;color: rgb(193,190,190);border: solid black; ;">Cancel</button><button class="btn btn-primary border-light shadow-lg" type="submit" style="background-color: #a8055d;">Save</button></div>
				</div>
                </form>
                <hr>
                 <form action="profile-edit02b" method="post"  >
                {{csrf_field()}}
                
                <div class="row">
                    <div class="col text-black-50 tiitle-info">
                        <h3>Link Account</h3>
                    </div>
                    <div class="col text-right title-info-edit"></div>
                </div>
                <div class="row" style="margin: 0px 0px 31px;">
                    <div class="col fb-link-acc"><img src="{{ url('images/001-facebook.svg') }}"></div>
                    <div class="col google-link-acc"><img src="{{ url('images/Group.svg') }}" style="max-width: 40px;max-height: 40px;"></div>
                </div>
                <hr>
                <div class="row">
                    <div class="col text-black-50 tiitle-info">
                        <h3>Password</h3>
                    </div>
                    
                </div>
                <div class="row">
                    <div class="col input-prf-col"><label>Old Password</label><input value="123456234"  type="password" name="now_password"  autofocus="">



                        <small >{{ $errors->first('now_password') }}</small>



                    </div>

                    <div class="col input-prf-col"><label>New Password</label><input value=""  type="password" name="password"  autofocus=""><small >{{ $errors->first('password') }}</small></div>
                </div>

                <div class="row">
                    
                    <div style="border-style: none;" class="col input-prf-col"></div>
                    <div class="col input-prf-col"><label>Retype New Password</label><input value=""  type="password" name="password_confirmation"  autofocus=""><small >{{ $errors->first('password_confirmation') }}</small></div>
                </div>
               
                <div class="row">
    				<div class="col"></div>
    				<div class="col text-right sve-btn-prf"><button class="btn btn-primary border-light shadow" type="button" onclick="window.location.href = '{{ route('profile')}}'" style="background-color: white;color: rgb(193,190,190); border: solid black;">Cancel</button><button class="btn btn-primary border-light shadow-lg" type="submit" style="background-color: #a8055d;">Save</button></div>
				</div>
                </form>
                


                 
               

@endsection



@section('customer-thing-menu')

                <div class="row">
                    <div class="col text-center prf-side-photo"><img class="border rounded-circle shadow-sm" src="{{ url('images/avatar.png') }}"></div>
                </div>
                <div class="row">
                    <div class="col side-menu-prf">
                        <ul>
                            <li>Hi,<strong> {{ Auth::user()->name }}</strong></li>
                        </ul>
                    </div>
                </div>
                <hr>

                @include('layouts.sidemenu-customer') 
@endsection