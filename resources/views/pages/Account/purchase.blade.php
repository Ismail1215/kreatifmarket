@extends('layouts.layout01')

@section('title', 'Purcashase')


@section('customer-thing')
<div class="row">
                    <div class="col text-white prf-title">
                        <h3>Purchase History</h3>
                    </div>
                </div>

                <div class="row">
                    <div class="col" style="text-align: right;font-size: 14px;font-weight: bold;margin-bottom: 30;margin-top: 10px;">
                        
                            <p>Latest <i class="fa fa-chevron-circle-down"></i>   2019 <i class="fa fa-chevron-circle-down"></i></p>
                        
                    </div>
                </div>
                
                <div class="row">
                    <div class="col">
                        <div class="table-responsive text-center shadow-sm">
                            <table class="table table-hover">
                                <tbody>
                                    <tr>
                                        <td style="font-weight: bold;font-size: 12px;font-family:'Roboto';">26 Dec 2018</td>
                                        <td style="font-size: 12px;font-family:'Roboto';">365-day Footage Subscription, Standard License with 10 downloads, HD Available</td>
                                        <td style="font-size: 12px;font-family:'Roboto';">MYR 550.00</td>
                                        <td style="font-size: 12px;font-family:'Roboto';color: red">KMTK‑0417E‑EC46</td>
                                        
                                    </tr>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
               <div class="row">
                    <div class="col">
                        <div class="table-responsive text-center shadow-sm">
                            <table class="table table-hover">
                                <tbody>
                                    <tr>
                                        <td style="font-weight: bold;font-size: 12px;font-family:'Roboto';">26 Dec 2018</td>
                                        <td style="font-size: 12px;font-family:'Roboto';">365-day Footage Subscription, Standard License with 10 downloads, HD Available</td>
                                        <td style="font-size: 12px;font-family:'Roboto';">MYR 550.00</td>
                                        <td style="font-size: 12px;font-family:'Roboto';color: red">KMTK‑0417E‑EC46</td>
                                        
                                    </tr>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col">
                        <div class="table-responsive text-center shadow-sm">
                            <table class="table table-hover">
                                <tbody>
                                    <tr>
                                        <td style="font-weight: bold;font-size: 12px;font-family:'Roboto';">26 Dec 2018</td>
                                        <td style="font-size: 12px;font-family:'Roboto';">365-day Footage Subscription, Standard License with 10 downloads, HD Available</td>
                                        <td style="font-size: 12px;font-family:'Roboto';">MYR 550.00</td>
                                        <td style="font-size: 12px;font-family:'Roboto';color: red">KMTK‑0417E‑EC46</td>
                                        
                                    </tr>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
               
                
                
                

@endsection



@section('customer-thing-menu')

                <div class="row">
                    <div class="col text-center prf-side-photo"><img class="border rounded-circle shadow-sm" src="{{ url('images/avatar.png') }}"></div>
                </div>
                <div class="row">
                    <div class="col side-menu-prf">
                        <ul>
                            <li>Hi,<strong> {{ Auth::user()->name }}</strong></li>
                        </ul>
                    </div>
                </div>
                <hr>

                @include('layouts.sidemenu-customer') 
@endsection