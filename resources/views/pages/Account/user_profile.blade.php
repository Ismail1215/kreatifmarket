@extends('layouts.layout01')

@section('title', 'User Profile')


@section('customer-thing')
            <div class="row">
                    <div class="col text-white prf-title">
                        <h3>My Profile</h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col text-black-50 tiitle-info">
                        <h3>Personal Information</h3>
                    </div>
                    <div class="col text-right title-info-edit"><a onclick="return confirm('Are you sure to edit this?')" href="{{ route('profile-edit')}}">edit</a></div>
                </div>
                 

                {{csrf_field()}}
                <div class="row">
                    <div class="col input-prf">
                        @foreach ($user_details as $data) 
                        <div class="row">
                            <div class="col input-prf-col"><label>First Name</label><input  value="{{ $data->name }}" type="text" name="name" readonly="" autofocus=""></div>
                        </div>
                        <div class="row">
                            <div class="col input-prf-col"><label>Last Name</label><input  value=" {{ $data->last_name }}" type="text" name="last_name" readonly="" autofocus=""></div>
                        </div>


                        <div class="row">
                            <div class="col input-prf-col"><label>Email</label><input  value=" {{ $data->email }}" type="text" name="email" readonly="" autofocus=""></div>
                        </div>
                        <div class="row">
                            <div class="col input-prf-col"><label>Contact No.</label><input  value=" {{ $data->phone }}" type="text" name="phone" readonly="" autofocus=""></div>
                        </div>
                        <div class="row">
                            <div class="col input-prf-col"><label>ID/IC/MyKAD No.</label><input  value=" {{ $data->ic }}" type="text" name="ic" readonly="" autofocus=""></div>
                        </div>
                    </div> 
                    @endforeach
                    <div class="col">
                        <div class="row">
                            <div class="col prf-photo">
                                <label style="float: left">Profile Photo</label>
                                <img  src="{{ url('images/avatar.png') }}">
                                
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col text-black-50 tiitle-info">
                        <h3>Link Account</h3>
                    </div>
                    <div class="col text-right title-info-edit"></div>
                </div>
                <div class="row" style="margin: 0px 0px 31px;">
                    <div class="col fb-link-acc"><img src="{{ url('images/001-facebook.svg') }}"></div>
                    <div class="col google-link-acc"><img src="{{ url('images/Group.svg') }}" style="max-width: 40px;max-height: 40px;"></div>
                </div>
                <hr>
                <div class="row">
                    <div class="col text-black-50 tiitle-info">
                        <h3>Password</h3>
                    </div>
                    <div class="col text-right title-info-edit"><a onclick="return confirm('Are you sure to edit this?')" href="{{ route('profile-edit')}}">edit</a></div>
                </div>
                <div class="row">
                    <div class="col input-prf-col"><label>Password</label><input value="12345678"  type="password" name="password" readonly="" autofocus=""></div>
                    <div class="col col-last-input-prf"></div>
                </div>
                

@endsection



@section('customer-thing-menu')

                <div class="row">
                    <div class="col text-center prf-side-photo"><img class="border rounded-circle shadow-sm" src="{{ url('images/avatar.png') }}"></div>
                </div>
                <div class="row">
                    <div class="col side-menu-prf">
                        <ul>
                            <li>Hi,<strong> {{ Auth::user()->name }}</strong></li>
                        </ul>
                    </div>
                </div>
                <hr>

                @include('layouts.sidemenu-customer') 
@endsection