@extends('layouts.layout01')

@section('title', 'User Profile')


@section('customer-thing')
				<div class="row">
                    <div class="col text-white prf-title">
                        <h3>Plans &amp; Pricings</h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col text-black-50 tiitle-info">
                        <h3>Credit</h3>
                    </div>
                    <div class="col text-right title-info-edit"></div>
                </div>
                <div class="row">
                    <div class="col text-left col-comment-parp"><span>Yout don't have any available credit plan</span></div>
                    <div class="col text-right col-comment-butn"><button class="btn btn-primary border-warning shadow" type="submit" style="background-color: #ffc700;">Get Credit</button></div>
                </div>
                <hr>
                <div class="row">
                    <div class="col text-black-50 tiitle-info">
                        <h3>Free Credit</h3>
                    </div>
                    <div class="col text-right title-info-edit"></div>
                </div>
                <div class="row">
                    <div class="col text-left col-comment-parp"><span>Yout don't have any available &nbsp;free credit&nbsp;</span></div>
                    <div class="col text-right col-comment-butn"></div>
                </div>
                <hr>
                <div class="row">
                    <div class="col text-black-50 tiitle-info">
                        <h3>Image Plan</h3>
                    </div>
                    <div class="col text-right title-info-edit"></div>
                </div>
                <div class="row">
                    <div class="col cor-card">
                        <div class="card crd-margin">
                            <div class="card-body mrgn">
                                <div class="row">
                                    <div class="col text-left img-plan-sub"><span><br>Monthly Subscription<br></span>
                                        <p>1-Month Subscription, Standard License with 750 Downloads per Month Standard License<br></p>
                                    </div>
                                    <div class="col-xl-3 text-left"></div>
                                </div>
                                <div class="row">
                                    <div class="col text-left img-plan-sub"><span>Plan Expires : 6 days <br></span>
                                        <p>Downloads Remaining: 105 / 750 This Month<br></p>
                                    </div>
                                    <div class="col-xl-3 text-right radio-btn-post"><img src="{{ url('images/radiobtn.svg') }}"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <hr>
                <div class="row">
                    <div class="col text-black-50 tiitle-info">
                        <h3>Video Plan</h3>
                    </div>
                    <div class="col text-right title-info-edit"></div>
                </div>
                <div class="row">
                    <div class="col cor-card">
                        <div class="card crd-margin">
                            <div class="card-body mrgn">
                                <div class="row">
                                    <div class="col text-left img-plan-sub"><span><br>HD Footage Subscription<br></span>
                                        <p>365-day Footage Subscription, Standard License with 5 downloads, HD Available<br></p>
                                    </div>
                                    <div class="col-xl-3 text-left"></div>
                                </div>
                                <div class="row">
                                    <div class="col text-left img-plan-sub"><span>Plan Expires : 205 days<br></span>
                                        <p>Downloads Remaining: 1 / 5<br></p>
                                    </div>
                                    <div class="col-xl-3 text-right radio-btn-post"><img src="{{ url('images/radiobtn.svg') }}"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col text-black-50 tiitle-info">
                        <h3>Audio Plan</h3>
                    </div>
                    <div class="col text-right title-info-edit"></div>
                </div>
                <div class="row row-last-mrgn">
                    <div class="col text-left col-comment-parp"><span>Yout don't have an active audio plan</span></div>
                    <div class="col text-right col-comment-butn"><button class="btn btn-primary border-warning shadow" type="submit" style="background-color: #ffc700;">Get Plan</button></div>
                </div>

@endsection



@section('customer-thing-menu')

                
                <div class="row">
                    <div class="col text-center prf-side-photo"><img class="border rounded-circle shadow-sm" src="{{ url('images/avatar.png') }}"></div>
                </div>
                <div class="row">
                    <div class="col side-menu-prf">
                        <ul>
                            <li>Hi,<strong> {{ Auth::user()->name }}</strong></li>
                        </ul>
                    </div>
                </div>
                <hr>

                @include('layouts.sidemenu-customer')  
@endsection