@extends('layouts.layout05')

@section('title', 'Profile')


@section('superadmin-thing')
                <div class="row">
                    <div class="col text-white prf-title">
                        <h3>Admin profile</h3>
                    </div>
                </div>


                <div class="row">
                    <div class="col">
                       <img style="min-width: 700px" src="{{ url('images/dummy/Capture4.PNG') }}">
                    </div>
                </div>
               
               

@endsection



@section('superadmin-thing-menu')

                <div class="row">
                    <div class="col text-center prf-side-photo"><img class="border rounded-circle shadow-sm" src="{{ url('images/avatar.png') }}"></div>
                </div>
                <div class="row">
                    <div class="col side-menu-prf">
                        <ul>
                            <li>Hi,<strong> Zara Akif</strong></li>
                        </ul>
                    </div>
                </div>
                <hr>

                @include('layouts.sidemenu-superadmin') 
@endsection