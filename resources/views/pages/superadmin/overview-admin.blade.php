@extends('layouts.layout05')

@section('title', 'Profile')


@section('superadmin-thing')
                <div class="row">
                    <div class="col text-white prf-title">
                        <h3>superadmin overview</h3>
                    </div>
                </div>
               
               

@endsection



@section('superadmin-thing-menu')

                <div class="row">
                    <div class="col text-center prf-side-photo"><img class="border rounded-circle shadow-sm" src="{{ url('images/Profile Photo2.svg') }}"></div>
                </div>
                <div class="row">
                    <div class="col side-menu-prf">
                        <ul>
                            <li>Hi,<strong> {{ Auth::user()->name }}</strong></li>
                        </ul>
                    </div>
                </div>
                <hr>

                @include('layouts.sidemenu-superadmin') 
@endsection