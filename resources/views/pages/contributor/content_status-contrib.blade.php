@extends('layouts.layout02')

@section('title', 'Overview')


@section('contributor-thing')

               
               <div class="row">
                    <div class="col text-white prf-title">
                        <h3>Content Status</h3>
                    </div>
                </div>

                <div class="row">
                    <div style="max-width: 30px" class="col">
                        <img src="{{ url('images/dummy/Capture2.PNG') }}">
                    </div>
                   
                </div>
               

@endsection



@section('contributor-thing-menu')

                <div class="row">
                    <div class="col text-center prf-side-photo"><img class="border rounded-circle shadow-sm" src="{{ url('images/avatar.png') }}"></div>
                </div>
                <div class="row">
                    <div class="col side-menu-prf">
                        <ul>
                            <li>Hi,<strong> {{ Auth::user()->name }}</strong></li>
                        </ul>
                    </div>
                </div>
                <hr>

                @include('layouts.sidemenu-contributor') 
@endsection