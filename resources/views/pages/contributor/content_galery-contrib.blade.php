@extends('layouts.layout02')

@section('title', 'Content Gallery')


@section('contributor-thing')

       <div class="row">
        <div class="col text-white prf-title">
            <h3>Content Gallery</h3>
        </div>
    </div>

    <div class="col" style="text-align: right;font-size: 14px;font-weight: bold;margin-bottom: 10px;margin-top: 0px;">
                        
                        
                        <span style="margin-right: 40px">Images <i class="fa fa-chevron-circle-down"></i>   2018 (Latest) <i class="fa fa-chevron-circle-down"></i></span> 
                        
                    </div>

       @include('layouts.dummy_image')         

@endsection



@section('contributor-thing-menu')

                <div class="row">
                    <div class="col text-center prf-side-photo"><img class="border rounded-circle shadow-sm" src="{{ url('images/avatar.png') }}"></div>
                </div>
                <div class="row">
                    <div class="col side-menu-prf">
                        <ul>
                            <li>Hi,<strong> {{ Auth::user()->name }}</strong></li>
                        </ul>
                    </div>
                </div>
                <hr>

                @include('layouts.sidemenu-contributor') 
@endsection