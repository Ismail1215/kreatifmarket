@extends('layouts.layout02')

@section('title', 'Overview')


@section('contributor-thing')


<div class="row">
        <div class="col text-white prf-title">
            <h3>Overview - Contributor</h3>
        </div>
    </div>
<div class="col" style="background-color: #ffffff;">
    
    <div class="row">
        <div class="col space-align">
            <div class="row col-bx2">
                <div class="col col-bx2-info"><label>Unpaid Earnings*</label><input type="text" value="MYR 0.00" />
                    <p>*Updated Approximately every 15minutes</p>
                </div>
                <div class="col-xl-5 text-left col-bx2-info2">
                    <p>Payments are calculated at the end of every month for contributors who meet their minimum payout amount.<br /></p><a href="#">Learn more</a></div>
            </div>
            <div class="row">
                <div class="col col-bx-1"><label>Total Earnings</label><input type="text" value="MYR 0.00" /></div>
                <div class="col col-bx-1"><label>Total Downloads</label><input type="text" value="MYR 0.00" /></div>
            </div>
            <div class="row">
                <div class="col col-bx-1"><label>Total Earnings This Month</label><input type="text" value="MYR 0.00" /></div>
                <div class="col col-bx-1"><label>Total Downloads This Month</label><input type="text" value="MYR 0.00" /></div>
            </div>
        </div>
        <div class="col tabel-span-title"><span>Top Downloads</span>
            <div class="table-responsive shadow-sm tble-setting1">
                <table class="table table-striped table-hover">
                    <thead>
                        <tr class="tble-header-color">
                            <th>Category</th>
                            <th>Content Name</th>
                            <th>Content ID</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Cell 1</td>
                            <td>Cell 2</td>
                            <td>Cell 2</td>
                        </tr>
                        <tr>
                            <td>Cell 3</td>
                            <td>Cell 2</td>
                            <td>Cell 4</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col tabel-span-title"><span>Content Upload Summary</span>
            <div class="table-responsive shadow-sm tble-setting1">
                <table class="table table-striped table-hover">
                    <thead>
                        <tr class="tble-header-color">
                            <th>Column 1</th>
                            <th>Image</th>
                            <th>Vedio</th>
                            <th>Audio</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Approved</td>
                            <td>Cell 2</td>
                            <td>Cell 2</td>
                            <td>Cell 2</td>
                        </tr>
                        <tr>
                            <td>Pending</td>
                            <td>Cell 2</td>
                            <td>Cell 2</td>
                            <td>Cell 4</td>
                        </tr>
                        <tr>
                            <td>Rejected</td>
                            <td>Cell 2</td>
                            <td>Cell 3</td>
                            <td>Cell 4</td>
                        </tr>
                        <tr>
                            <td>Uploaded</td>
                            <td>Cell 2</td>
                            <td>Cell 3</td>
                            <td>Cell 4</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="col tabel-span-title"><span>Recent Downloads (Last 30 Days)</span>
            <div class="table-responsive shadow-sm tble-setting1">
                <table class="table table-striped table-hover">
                    <thead>
                        <tr class="tble-header-color">
                            <th>Category</th>
                            <th>Content Name</th>
                            <th>Content ID</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Cell 1</td>
                            <td>Cell 2</td>
                            <td>Cell 2</td>
                        </tr>
                        <tr>
                            <td>Cell 3</td>
                            <td>Cell 2</td>
                            <td>Cell 4</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
               
              


               

@endsection



@section('contributor-thing-menu')

                <div class="row">
                    <div class="col text-center prf-side-photo"><img class="border rounded-circle shadow-sm" src="{{ url('images/avatar.png') }}"></div>
                </div>
                <div class="row">
                    <div class="col side-menu-prf">
                        <ul>
                            <li>Hi,<strong> {{ Auth::user()->name }}</strong></li>
                        </ul>
                    </div>
                </div>
                <hr>

                @include('layouts.sidemenu-contributor') 
@endsection