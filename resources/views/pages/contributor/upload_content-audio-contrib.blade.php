@extends('layouts.layout02a')

@section('title', 'Upload Content') 

@section('contributor-thing2a')
                    <div class="row">
                        <div class="col col-title-box">
                            <h1>Upload Content</h1>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col col-btn-step"><img src="{{ url('images/uploadcontent/Group%202.png') }}"></div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col col-head-menu-inside">
                            <h1>Select type of content</h1>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col col-btn-step-detail">

                            <a href="{{ route('upload_content-contributor')}}">
                                <img src="{{ url('images/uploadcontent/photoblack.png') }}">
                            </a>
                            <a href="#">
                                <img src="{{ url('images/uploadcontent/audiogreen.png') }}">
                            </a>
                            <a href="{{ route('upload_content_video-contributor')}}">
                                <img src="{{ url('images/uploadcontent/Group%2019.png') }}">
                            </a>
                            
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col column-btn-next">
                            <button class="btn btn-primary shadow-sm"  type="button">Next</button>
                        </div>
                    </div>

@endsection

@section('contributor-thing-menu2a')

                   @foreach ($user_details as $data) 

                    <div class="col-xl-3 menu-list">

                        @if($data->img_prf)

                            <img src="{{ url('') }}/{{ $data->img_prf }}">            

                                
                        @else

                            <img src="{{ url('images/avatar.png') }}">                              
                                                                
                        @endif

                        
                    <h1>Hi, <strong>{{ Auth::user()->name }}</strong></h1>
                    <span>Basic &amp; Contributor</span>
                    @endforeach

@include('layouts.sidemenu-contributor2a') 

                    </div>


@endsection