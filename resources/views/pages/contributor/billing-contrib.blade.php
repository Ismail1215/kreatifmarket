@extends('layouts.layout02')

@section('title', 'Billings') 


@section('contributor-thing')
<div class="row">
                    <div class="col text-white prf-title">
                        <h3>Billings</h3>
                    </div>
                </div>
                 {{csrf_field()}}
                <div class="row">
                    <div class="col text-black-50 tiitle-info">
                        <h3>Payment Method</h3>
                    </div>
                    <div class="col text-right title-info-edit"></div>
                </div>

                <div class="row">
                    <div class="col">
                        <div class="table-responsive text-center shadow-sm">
                            <table class="table table-hover">
                                <tbody>
                                    <tr>
                                        <td style="font-weight: bold;font-size: 14px;">Payment Method 1</td>
                                        <td><img src="{{ url('images/dummy/Groupbank.png') }}"></td>
                                        <td style="font-weight: bold;font-size: 14px;">**** **** **** 1607 </td>
                                        <td style="font-weight: bold;font-size: 14px;">Expires 02/2019</td>
                                        <td style="color: red;font-size: 14px;">edit delete</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

               <div class="row">
                    <div class="col">
                        <div class="table-responsive text-center shadow-sm">
                            <table class="table table-hover">
                                <tbody>
                                    <tr>
                                        <td style="font-weight: bold;font-size: 14px;">Payment Method 2</td>
                                        <td><img src="{{ url('images/dummy/visa.png') }}"></td>
                                        <td style="font-weight: bold;font-size: 14px;">**** **** **** 1607 </td>
                                        <td style="font-weight: bold;font-size: 14px;">Expires 02/2019</td>
                                        <td style="color: red;font-size: 14px;">edit delete</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col btn-billing"><button class="btn btn-light" onclick="window.location.href = '{{ route('billings-contributor-edit')}}'" type="button">Add Payment Method +</button></div>
                </div>

                <hr />

                <div class="row">
                    <div class="col text-black-50 tiitle-info">
                        <h3>Billing Address</h3>
                    </div>
                    <div class="col text-right title-info-edit"></div>
                </div>
                 @foreach ($user_details as $data)
                <div class="row">
                     <div class="col card-address">
                          <div class="card">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col address">
                                             <h3>{{ $data->company }}</h3>
                        <p>{{ $data->name }}  {{ $data->last_name }}</p>
                        <p>{{ $data->line1 }} </p>
                        <p>{{ $data->line2 }}</p>
                        <p>{{ $data->line3 }}</p>
                   
                        <p>{{ $data->city }} {{ $data->postcode }}</p>
                        <p>{{ $data->state }} {{ $data->country }}</p>
                                            

                                        </div>
                                        <div class="col address-edit">
                                         <a onclick="return confirm('Are you sure to edit this?')" href="{{ route('billings-contributor-edit')}}"> edit</a></div>
                                    </div>
                                </div>
                           </div>
                        </div>
                </div>
                 @endforeach
               

@endsection



@section('contributor-thing-menu')

                <div class="row">
                    <div class="col text-center prf-side-photo"><img class="border rounded-circle shadow-sm" src="{{ url('images/avatar.png') }}"></div>
                </div>
                <div class="row">
                    <div class="col side-menu-prf">
                        <ul>
                            <li>Hi,<strong> {{ Auth::user()->name }}</strong></li>
                        </ul>
                    </div>
                </div>
                <hr>

                @include('layouts.sidemenu-contributor') 
@endsection