@extends('layouts.layout02')

@section('title', 'Overview')


@section('contributor-thing')
<div class="row">
                    <div class="col text-white prf-title">
                        <h3>Overview</h3>
                    </div>
                </div>
               
               <div class="row">
                    <div class="col text-white prf-title">
                        <h3>helpdesk-Contributor</h3>
                    </div>
                </div>
               

@endsection



@section('contributor-thing-menu')

                <div class="row">
                    <div class="col text-center prf-side-photo"><img class="border rounded-circle shadow-sm" src="assets/img/body/Profile%20Photo.svg"></div>
                </div>
                <div class="row">
                    <div class="col side-menu-prf">
                        <ul>
                            <li>Hi,<strong> {{ Auth::user()->name }}</strong></li>
                        </ul>
                    </div>
                </div>
                <hr>

                @include('layouts.sidemenu-contributor') 
@endsection