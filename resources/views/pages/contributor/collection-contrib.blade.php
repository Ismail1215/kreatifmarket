@extends('layouts.layout02')

@section('title', 'Overview')


@section('contributor-thing')
<div class="row">
        <div class="col text-white prf-title">
            <h3>Collections</h3>
        </div>
    </div>
    <div class="col" style="text-align: left;font-size: 14px;font-weight: bold;margin-bottom: 30px;margin-top: 0px;">
                        
                        <span style="margin-left: 20px; margin-right: 350px">You have 4 Collections </span>
                        
                        
                    </div>


                    <div class="row rowgrid1">
                        <div class="col-lg-4 gridset1">
                            <div class="row">
                             <div class="col-lg-4 col-xl-10 gridset1"><img src="{{ url('images/dummy/newcol.png') }}" /></div>
                         </div>
                     </div>

                     <div class="col-lg-4 gridset1">
                            <div class="row">
                             <div class="col-lg-4 col-xl-10 gridset1"><a href="{{ route('likes-contributor')}}"><img src="{{ url('images/dummy/whistls.png') }}" /></a></div>
                         </div>
                     </div>
                     <div class="col-lg-4 gridset1">
                            <div class="row">
                             <div class="col-lg-4 gridset1"><img src="{{ url('images/dummy/col1.png') }}" /></div>
                            </div>
                     </div>
                      <div class="col-lg-4 gridset1">
                         <div class="row">
                              <div class="col-lg-4 gridset1"><img src="{{ url('images/dummy/col2.png') }}" /></div>
                          </div>
                      </div>
                      <div class="col-lg-4 gridset1">
                          <div class="row">
                                <div class="col-lg-4 gridset1"><img src="{{ url('images/dummy/col3.png') }}" /></div>
                          </div>
                       </div>
                        <div class="col-lg-4 gridset1">
                          <div class="row">
                                <div class="col-lg-4 gridset1"><img src="{{ url('images/dummy/col4.png') }}" /></div>
                           </div>
                        </div>
                    </div>
               

@endsection



@section('contributor-thing-menu')

                <div class="row">
                    <div class="col text-center prf-side-photo"><img class="border rounded-circle shadow-sm" src="{{ url('images/avatar.png') }}"></div>
                </div>
                <div class="row">
                    <div class="col side-menu-prf">
                        <ul>
                            <li>Hi,<strong> {{ Auth::user()->name }}</strong></li>
                        </ul>
                    </div>
                </div>
                <hr>

                @include('layouts.sidemenu-contributor') 
@endsection