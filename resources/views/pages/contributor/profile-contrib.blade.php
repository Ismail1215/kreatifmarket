@extends('layouts.layout02')

@section('title', 'Profile')


@section('contributor-thing')
                <div class="row">
                    <div class="col text-white prf-title">
                        <h3>Artistry Profile</h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col text-black-50 tiitle-info">
                        <h3>Personal Information</h3>
                    </div>
                    <div class="col text-right title-info-edit"><a onclick="return confirm('Are you sure to edit this?')" href="{{ route('profile-contrib-edit')}}">edit</a></div>
                </div>
               {{csrf_field()}}
                <div class="row">
                    <div class="col input-prf">
                         @foreach ($user_details as $data) 
                        <div class="row">
                            <div class="col input-prf-col"><label>First Name</label><input  value="{{ $data->name }}" type="text" name="name" readonly="" autofocus=""></div>
                        </div>
                        <div class="row">
                            <div class="col input-prf-col"><label>Last Name</label><input  value=" {{ $data->last_name }}" type="text" name="last_name" readonly="" autofocus=""></div>
                        </div>
                        <div class="row">
                            <div class="col input-prf-col"><label>Username</label><input  value=" {{ $data->username }}" type="text" name="username" readonly="" autofocus=""></div>
                        </div>
                        <div class="row">
                            <div class="col input-prf-col"><label>Email</label><input  value=" {{ $data->email }}" type="text" name="email" readonly="" autofocus=""></div>
                        </div>
                        <div class="row">
                            <div class="col input-prf-col"><label>Contact No.</label><input  value=" {{ $data->phone }}" type="text" name="phone" readonly="" autofocus=""></div>
                        </div>
                        <div class="row">
                            <div class="col input-prf-col"><label>ID/IC/MyKAD No.</label><input  value=" {{ $data->ic }}" type="text" name="ic" readonly="" autofocus=""></div>
                        </div>
                    </div>

                    <div class="col">
                        <div class="row">


                            @if($data->img_prf)
                                <div class="col prf-photo">
                                <label style="float: left">Profile Photo</label>
                                <img  src="{{ url('') }}/{{ $data->img_prf }}">
                                
                                </div>
                            @else

                                <div class="col prf-photo">
                                <label style="float: left">Profile Photo</label>
                                <img  src="{{ url('images/avatar.png') }}">
                                
                                </div>
                                                                
                             @endif


                            
                        </div>


                         <div class="row">   

                                <div class="col input-prf-col-right"><label>About Contributor (0 / 150 characters)</label><br><textarea cols="55" rows="4" charswidth="23" name="textbody" placeholder="enter text here..">{{ $data->desc_contrib }}</textarea></div>
                        </div>


                        <div class="row">   

                                <div class="col input-prf-col"><label>Interest</label><input  value="{{ $data->interest }}" type="text" name="ic" readonly="" autofocus=""></div>

                        </div>
                    </div>
                </div>
                
                <hr>
                <div class="row">
                    <div class="col text-black-50 tiitle-info">
                        <h3>Link Account</h3>
                    </div>
                    <div class="col text-right title-info-edit"></div>
                </div>
                <div class="row" style="margin: 0px 0px 25px;">
                    <div class="col fb-link-acc"><img src="{{ url('images/001-facebook.svg') }}"></div>
                    <div class="col google-link-acc"><img src="{{ url('images/Group.svg') }}" style="max-width: 40px;max-height: 40px;"></div>
                </div>
                <hr>
                <div class="row">
                    <div class="col text-black-50 tiitle-info">
                        <h3>Password</h3>
                    </div>
                    <div class="col text-right title-info-edit"><a onclick="return confirm('Are you sure to edit this?')" href="{{ route('profile-contrib-edit')}}">edit</a></div>
                </div>
                <div class="row">
                    <div class="col input-prf-col"><label>Password</label><input value="12345678"  type="password" name="password" readonly="" autofocus=""></div>
                    <div class="col "></div>
                </div>

                 <hr>
                <div class="row">
                    <div class="col text-black-50 tiitle-info">
                        <h3>Address</h3>
                    </div>
                    <div class="col text-right title-info-edit"><a onclick="return confirm('Are you sure to edit this?')" href="{{ route('profile-contrib-edit')}}">edit</a></div>
                </div>
                <div class="row">
                    <div class="col input-address-prf-col">
                        <h3>{{ $data->company }}</h3>
                        <p>{{ $data->name }}  {{ $data->last_name }}</p>
                        <p>{{ $data->line1 }} </p>
                        <p>{{ $data->line2 }}</p>
                        <p>{{ $data->line3 }}</p>
                   
                        <p>{{ $data->city }} {{ $data->postcode }}</p>
                        <p>{{ $data->state }} {{ $data->country }}</p>
                      
                                                
                        
                        
                    </div>
                    
                </div>

                 <hr>
                <div class="row">
                    <div class="col text-black-50 tiitle-info">
                        <h3>Kreatifmarket URL</h3>
                    </div>
                     @if (( $data->status_prt ) == 1)
                   

                     @else


                      <div class="col text-right title-info-edit"><a onclick="return confirm('Are you sure to edit this?')" href="{{ route('profile-contrib-edit')}}">edit</a></div>

                     @endif
                </div>




                 @if (( $data->status_prt ) == 1)



                  <div class="row">
                    <div class="col input-prf-col"><label>Website</label><input value="{{ $data->url_portfolio }}"  type="text" name="password" readonly="" autofocus=""></div>

                    <div class="col input-prf-col" style="border-style: none;">
                        <p>Note:</p>
                        <span>You already changed Portfolio URL</span>
                    </div>
                </div>


                


                

                @else


                 <div class="row">
                    <div class="col input-prf-col"><label>Website</label><input value="http://kreatifmarket.com/{{ $data->username }}"  type="text" name="password" readonly="" autofocus=""></div>

                    <div class="col input-prf-col" style="border-style: none;">
                        <p>Note:</p>
                        <span>Your Portfolio URL can only be changed once</span>
                    </div>
                </div>

                   
                @endif


               
                 <hr>
                <div class="row">
                    <div class="col text-black-50 tiitle-info">
                        <h3>Additional Contact Details</h3>
                    </div>
                    <div class="col text-right title-info-edit"><a onclick="return confirm('Are you sure to edit this?')" href="{{ route('profile-contrib-edit')}}">edit</a></div>
                </div>


                <div class="row">
                    <div class="col input-prf-col"><label>Website</label><input placeholder="www." value="{{ $data->url_website }}"  type="text" name="website" readonly="" autofocus=""></div>
                    <div class="col input-prf-col"><label>Twitter</label><input placeholder="@" value="{{ $data->twitter_contrib }}"  type="text" name="twitter" readonly="" autofocus=""></div>
                </div>
                <div class="row">
                    <div class="col input-prf-col"><label>Instagram</label><input placeholder="@"  value="{{ $data->ig_contrib }}"  type="text" name="insta" readonly="" autofocus=""></div>
                    <div class="col input-prf-col"><label>Facebook</label><input placeholder="http://facebook.com/" value="{{ $data->fb_contrib }}"  type="text" name="fb" readonly="" autofocus=""></div>
                </div>
                <div class="row">
                    <div class="col input-prf-col"><label>Google+</label><input placeholder="http://plus.google.com/" value="{{ $data->gmail_contrib }}"  type="text" name="google" readonly="" autofocus=""></div>
                    <div class="col input-prf-col"><label>LinkedIn</label><input placeholder="http://linkedin.com/in/" value="{{ $data->linked_contrib }}"  type="text" name="linked" readonly="" autofocus=""></div>
                </div>

                 <hr>
                <div class="row">
                    <div class="col text-black-50 tiitle-info">
                        <h3>Bank Information</h3>
                    </div>
                    <div class="col text-right title-info-edit"><a onclick="return confirm('Are you sure to edit this?')" href="{{ route('profile-contrib-edit')}}">edit</a></div>
                </div>
                <div class="row" style="margin-bottom: 30px;">
                    <div class="col input-prf-col"><label>Bank</label><input value="{{ $data->bank_name }}"  type="text" name="bank_name" readonly="" autofocus=""></div>
                    <div class="col input-prf-col"><label>Account No.</label><input value="{{ $data->bank_acc }}"  type="text" name="bank_acc" readonly="" autofocus=""></div>
                </div>

                @endforeach
               

@endsection



@section('contributor-thing-menu')

                <div class="row">

                    @if($data->img_prf)
                        <div class="col text-center prf-side-photo"><img class="border rounded-circle shadow-sm" src="{{ url('') }}/{{ $data->img_prf }}"></div>
                    @else
                         <div class="col text-center prf-side-photo"><img class="border rounded-circle shadow-sm" src="{{ url('images/avatar.png') }}"></div>
                    @endif
                    
                </div>
                <div class="row">
                    <div class="col side-menu-prf">
                        <ul>
                            <li>Hi,<strong> {{ Auth::user()->name }}</strong></li>
                        </ul>
                    </div>
                </div>
                <hr>

                @include('layouts.sidemenu-contributor') 
@endsection