@extends('layouts.layout02a')

@section('title', 'Image') 

@section('contributor-thing2a')

                <form action="upload_content_send_img" method="post" enctype="multipart/form-data">
                {{csrf_field()}}   

                    <div class="row">
                        <div class="col col-title-box">
                            <h1>Upload Content</h1>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col col-btn-step"><img src="{{ url('images/uploadcontent/Group%202.1.png') }}"></div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col col-image-upload">
                            <h1>Upload Content</h1>
                            <div class="row">
                                <div class="col col-image-upload" id="filename"><img src="{{ url('images/uploadcontent/Vector.png') }}">
                                    <p>Upload multiple content by simply</p>
                                    <p>drag and drop from your device</p>
                                </div>
                            </div><input type="file" name="filename[]" accept="image/gif,image/jpeg" multiple="" required=""></div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col column-btn-back">
                            <button class="btn btn-primary border-dark shadow-sm" onclick="window.location.href = '{{ route('upload_content-contributor')}}'" type="button">Back</button>
                        </div>
                        <div class="col column-btn-next">
                            <button class="btn btn-primary shadow-sm" onclick="window.location.href = '#'" type="submit">Next</button>
                        </div>
                    </div>
                </form>

@endsection

@section('contributor-thing-menu2a')

                    @foreach ($user_details as $data) 

                    <div class="col-xl-3 menu-list">

                        @if($data->img_prf)

                            <img src="{{ url('') }}/{{ $data->img_prf }}">            
 
                        @endif

                        
                    <h1>Hi, <strong>{{ Auth::user()->name }}</strong></h1>
                    <span>Basic &amp; Contributor</span>
                    @endforeach

@include('layouts.sidemenu-contributor2a') 

                    </div>


@endsection