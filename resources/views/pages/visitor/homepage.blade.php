<!DOCTYPE html>
<html>

<head>
     <title>home</title>
    @include('layouts.header-visitor')
</head>

<body>
    <section id="sec-head">
        <nav class="navbar navbar-dark navbar-expand-md sticky-top flex-grow-1">
            <div class="container-fluid"><img src="{{ url('images/navbar1/Logo.svg') }}"><button data-toggle="collapse" class="navbar-toggler" data-target="#navcol-1"><span class="sr-only">Toggle navigation</span><span class="navbar-toggler-icon"></span></button>
                <div class="collapse navbar-collapse"
                    id="navcol-1">
                    <ul class="nav navbar-nav flex-grow-1 justify-content-between">
                        <li class="nav-item dropdown"><a class="dropdown-toggle nav-link" data-toggle="dropdown" aria-expanded="false" href="#">image</a>
                            <div  class="dropdown-menu" role="menu"><a style="color: #000000 !important;" class="dropdown-item" role="presentation" href="#">First Item</a><a style="color: #000000 !important;" class="dropdown-item" role="presentation" href="#">Second Item</a><a style="color: #000000 !important;" class="dropdown-item" role="presentation" href="#">Third Item</a></div>
                        </li>
                        <li class="nav-item" role="presentation"><input type="search"><i class="fa fa-search"></i></li>
                        <li class="nav-item dropdown"><a class="dropdown-toggle nav-link active" data-toggle="dropdown" aria-expanded="false" href="#">Discover</a>
                            <div class="dropdown-menu" role="menu"><a style="color: #000000 !important;" class="dropdown-item" role="presentation" href="#">First Item</a><a style="color: #000000 !important;" class="dropdown-item" role="presentation" href="#">Second Item</a><a style="color: #000000 !important;" class="dropdown-item" role="presentation" href="#">Third Item</a></div>
                        </li>
                        <li class="nav-item" role="presentation"><a class="nav-link active" href="{{ route('planpricepack')}}">Plan &amp; Pricing</a></li>
                        <li class="nav-item" role="presentation"><a class="nav-link active" href="#">Become Contributor</a></li>
                        <li class="nav-item" role="presentation"><button class="btn btn-primary" type="button">Login</button></li>
                        <li class="nav-item" role="presentation"><button class="btn btn-primary" type="button">Sign up</button></li>
                    </ul>
                </div>
            </div>
        </nav>
        <div class="container" id="search992">
            <div class="row row-search-992">
                <div class="col col-search-title">
                    <h1 data-bs-hover-animate="pulse">Discover over 1,000 royalty-free images</h1><span data-bs-hover-animate="flash">Creative &amp; digital content with a local flavour</span></div>
            </div>
            <div class="row row-search-992">
                <div class="col col-search-inp"><input type="search"></div>
            </div>
            <div class="row row-search-992">
                <div class="col col-search-title2"><span data-bs-hover-animate="flash"><strong>Trending searches</strong>: bussiness,computer, nature, love, house<br></span></div>
            </div>
        </div>
        <div class="container" id="search">
            <div class="row">
                <div class="col-xl-3"></div>
                <div class="col col-search-title">
                    <h1 data-bs-hover-animate="pulse">Discover over 1,000 royalty-free images</h1><span data-bs-hover-animate="flash">Creative &amp; digital content with a local flavour</span></div>
                <div class="col-xl-3"></div>
            </div>
            <div class="row">
                <div class="col-xl-3"></div>
                <div class="col col-search-inp"><input type="search"></div>
                <div class="col-xl-3"></div>
            </div>
            <div class="row">
                <div class="col-xl-3"></div>
                <div class="col col-search-title2"><span data-bs-hover-animate="flash"><strong>Trending searches</strong>: bussiness,computer, nature, love, house<br></span></div>
                <div class="col-xl-3"></div>
            </div>
        </div>
    </section>
    <section id="stunning">
        <div class="container">
            <div class="row">
                <div class="col col-stunt"><span>Stunning content, from as low as RM1.20 &nbsp; |</span><a href="#">find your plan &nbsp;<i class="fa fa-chevron-circle-right"></i></a></div>
            </div>
        </div>
    </section>
    <div class="container">
        <div class="row">
            <div class="col col-title">
                <h1>Find amazing local content for your next project<br></h1>
            </div>
        </div>
        <div class="row">
            <div class="col text-center col-menu"><button class="btn btn-primary" data-bs-hover-animate="pulse" type="button" style="background-image: url('images/homepage/photo.png');">Photo</button><button class="btn btn-primary" data-bs-hover-animate="pulse" type="button" style="background-image: url('images/homepage/illustration.png');background-position: center;background-size: cover;background-repeat: no-repeat;">Illustration</button>
                <button
                    class="btn btn-primary" data-bs-hover-animate="pulse" type="button" style="background-image: url('images/homepage/mockup.png');">Mockups</button><button class="btn btn-primary" data-bs-hover-animate="pulse" type="button" style="background-image: url('images/homepage/template.png');background-position: center;background-size: cover;background-repeat: no-repeat;">Templates</button>
                    <button
                        class="btn btn-primary" data-bs-hover-animate="pulse" type="button" style="background-image: url('images/homepage/vedio.png');background-position: center;background-size: cover;background-repeat: no-repeat;">Videos</button><button class="btn btn-primary" data-bs-hover-animate="pulse" type="button" style="background-image: url('images/homepage/audio.png');background-position: center;background-size: cover;background-repeat: no-repeat;">Audios</button></div>
            <div
                class="col screen430"><button class="btn btn-primary" type="button">Button</button><button class="btn btn-primary" type="button">Button</button><button class="btn btn-primary" type="button">Button</button><button class="btn btn-primary" type="button">Button</button>
                <button
                    class="btn btn-primary" type="button">Button</button><button class="btn btn-primary" type="button">Button</button></div>
    </div>
    <hr>
    <div class="row trending">
        <div class="col col-left"><span style="color: black;font-weight: bold;text-shadow: 1px 1px 2px rgb(166,165,165);"><strong>Trending</strong></span><span>Top Download &nbsp;|</span><a href="#">People</a><a href="#">Festival</a><a href="#">Business</a><a href="#">Cityscape</a></div>
        <div
            class="col col-right"><span>View more Categories</span><a href="#"><i class="fa fa-chevron-circle-right"></i></a></div>
    </div>
    <div class="row" id="hide">
        <div class="col">
            <div class="gallery">
                <figure class="figure gallery__item gallery__item--1" data-aos="flip-down" data-aos-duration="600"><img class="img-fluid figure-img gallery__img" data-bs-hover-animate="pulse" alt="Image 1" src="{{ url('images/homepage/dummy6-1.jpg') }}"></figure>
                <figure class="figure gallery__item gallery__item--2" data-aos="flip-right" data-aos-duration="600"><img class="img-fluid figure-img gallery__img" data-bs-hover-animate="pulse" alt="Image 2" src="{{ url('images/homepage/dummy6-1.jpg') }}"></figure>
                <figure class="figure gallery__item gallery__item--3" data-aos="flip-down" data-aos-duration="600"><img class="img-fluid figure-img gallery__img" data-bs-hover-animate="pulse" alt="Image 3" src="{{ url('images/homepage/dummy6-1.jpg') }}"></figure>
                <figure class="figure gallery__item gallery__item--4" data-aos="flip-up" data-aos-duration="600"><img class="img-fluid figure-img gallery__img" data-bs-hover-animate="pulse" alt="Image 3" src="{{ url('images/homepage/dummy6-1.jpg') }}"></figure>
                <figure class="figure gallery__item gallery__item--5" data-aos="flip-down" data-aos-duration="600"><img class="img-fluid figure-img gallery__img" data-bs-hover-animate="pulse" alt="Image 5" src="{{ url('images/homepage/dummy6-1.jpg') }}"></figure>
                <figure class="figure gallery__item gallery__item--6" data-aos="flip-left" data-aos-duration="600"><img class="img-fluid figure-img gallery__img" data-bs-hover-animate="pulse" alt="Image 6" src="{{ url('images/homepage/dummy6-1.jpg') }}"></figure>
                <figure class="figure gallery__item gallery__item--7" data-aos="flip-up" data-aos-duration="600"><img class="img-fluid figure-img gallery__img" data-bs-hover-animate="pulse" alt="Image 7" src="{{ url('images/homepage/dummy6-1.jpg') }}"></figure>
                <figure class="figure gallery__item gallery__item--8" data-aos="flip-down" data-aos-duration="600"><img class="img-fluid figure-img gallery__img" data-bs-hover-animate="pulse" alt="Image 8" src="{{ url('images/homepage/dummy6-1.jpg') }}"></figure>
                <figure class="figure gallery__item gallery__item--9" data-aos="flip-left" data-aos-duration="600"><img class="img-fluid figure-img gallery__img" data-bs-hover-animate="pulse" alt="Image 9" src="{{ url('images/homepage/dummy6-1.jpg') }}"></figure>
                <figure class="figure gallery__item gallery__item--10" data-aos="flip-down" data-aos-duration="600"><img class="img-fluid figure-img gallery__img" data-bs-hover-animate="pulse" alt="Image 10" src="{{ url('images/homepage/dummy6-1.jpg') }}"></figure>
                <figure class="figure gallery__item gallery__item--11" data-aos="flip-right" data-aos-duration="600"><img class="img-fluid figure-img gallery__img" data-bs-hover-animate="pulse" alt="Image 11" src="{{ url('images/homepage/dummy6-1.jpg') }}"></figure>
                <figure class="figure gallery__item gallery__item--12" data-aos="flip-up" data-aos-duration="600"><img class="img-fluid figure-img gallery__img" data-bs-hover-animate="pulse" alt="Image 12" src="{{ url('images/homepage/dummy6-1.jpg') }}"></figure>
                <figure class="figure gallery__item gallery__item--13" data-aos="flip-down" data-aos-duration="600"><img class="img-fluid figure-img gallery__img" data-bs-hover-animate="pulse" alt="Image 13" src="{{ url('images/homepage/dummy6-1.jpg') }}"></figure>
            </div>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col col-homepage-article">
            <h1>Kreatifmarket's Blogs</h1>
        </div>
        <div class="col col-right"><span>More articles &amp; Blogs</span><a href="#"><i class="fa fa-chevron-circle-right"></i></a></div>

    </div>
    </div>
    <section>
        <div class="container">
            <div class="row">
                <div class="col"><img src="{{ url('images/homepage/Capture.PNG') }}" style="margin-left: 131px;margin-top: 30px;margin-bottom: 50px;" /></div>
            </div>
        </div>
    </section>

    <section id="footer">
        @include('layouts.footer1')        
    </section>

    <section id="footer-merge">
        @include('layouts.footer1-merge')       
    </section>


    <script type="text/javascript" src="{{URL::asset('js/jquery.min.js')}}"></script>
    <script type="text/javascript" src="{{URL::asset('js/bootstrap.min.js')}}"></script>

  
    
    <script type="text/javascript" src="{{URL::asset('js/bs-animation.js')}}"></script>
    <script type="text/javascript" src="{{URL::asset('js/aos.js')}}"></script>
    
</body>

</html>