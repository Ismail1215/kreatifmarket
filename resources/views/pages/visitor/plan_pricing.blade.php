<!DOCTYPE html>
<html>

<head>
    <title>Plan Package</title>
    @include('layouts.header-visitor')
</head>

<body>
    <div id="navbar-collaspe" class="navbar sticky-top">
        <div class="container container-nv-collps">
            <div class="collapse-logo"><img src="assets/img/navbar1/Logo.svg"></div>
            <div class="search-collapse"><input type="search"><img src="{{ url('images/navbar1/Hover%202.png') }}"></div>
            <div class="collapse-buttn"><img src="assets/img/navbar1/Shape.png"></div>
        </div>
    </div>
    <div id="navbar-normal" class="navbar sticky-top">
        <div class="container">
            <div class="menuu-logo"><a href="{{ route('homepage')}}"><img src="{{ url('images/navbar1/Logo.svg') }}" href="#"></a></div>
            <div class="dropdown show"><a class="text-white btn btn-transparent dropdown-toggle" id="dropdownMenuLink" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="font-size: 15px;font-family: 'Roboto';">Images</a>
                <div class="dropdown-menu"
                    aria-labelledby="dropdownMenuLink"><a class="dropdown-item" href="#">Link</a><a class="dropdown-item" href="#">Link</a><a class="dropdown-item" href="#">Link</a><a class="dropdown-item" href="#">Link</a></div>
            </div>
            <div class="menuu-search"><input type="search" placeholder="search"><a href="#"><img src="{{ url('images/navbar1/Hover%202.png') }}"></a></div>
            <div class="dropdown show"><a class="text-white btn btn-transparent dropdown-toggle" id="dropdownMenuLink" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="font-size: 14px;font-family: 'Roboto';">Discover</a>
                <div class="dropdown-menu"
                    aria-labelledby="dropdownMenuLink"><a class="dropdown-item" href="#">Link</a><a class="dropdown-item" href="#">Link</a><a class="dropdown-item" href="#">Link</a><a class="dropdown-item" href="#">Link</a></div>
            </div>
            <div class="menuu">
                <ul>
                    <li><a href="#">Become Contributor</a></li>
                    <li><a href="#">Plans &amp; Pricing</a></li>
                </ul>
            </div>
            <div class="dropdown show"><a class="text-white btn btn-transparent dropdown-toggle" id="dropdownMenuLink" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="font-size: 14px;font-family: 'Roboto';">My Account</a>
                <div class="dropdown-menu"
                    aria-labelledby="dropdownMenuLink"><a class="dropdown-item" href="#">Link</a><a class="dropdown-item" href="#">Link</a><a class="dropdown-item" href="#">Link</a><a class="dropdown-item" href="#">Link</a></div>
            </div>
        </div>
    </div>
    <section id="section1">
        <div class="container">
            <div class="row row-ppp1-space">
                <div class="col col-ppp-sec1">
                    <h1 data-bs-hover-animate="flash">Checkout the plans &amp; pricings</h1>
                    <p data-bs-hover-animate="flash">Join &nbsp;Kreatifmarket's community of contributors and earn money doing what you love</p><button class="btn btn-primary" data-bs-hover-animate="pulse" type="button">Get Started<i class="fa fa-chevron-circle-right"></i></button></div>
            </div>
        </div>
    </section>
    <section id="ppp-sec2">
        <div class="container">
            <div class="row row-ppp2-space">
                <div class="col col-ppp-sec2">
                    <h1 class="pulse animated infinite">Easy Pack Plan</h1>
                    <p>Kreatifmarket is a place where you can market your art works captured through the creativity of your mind or the lens of your camera. Your creativity is your extra cash. Why not earn them by joining us now. And yes, get other benefits
                        too!<br></p>
                </div>
                <div class="col col-ppp-sec2">
                    <h1 class="bounce animated infinite">Subscription Plan</h1>
                    <p>Kreatifmarket is a place where you can market your art works captured through the creativity of your mind or the lens of your camera. Your creativity is your extra cash. Why not earn them by joining us now. And yes, get other benefits
                        too!<br></p>
                </div>
                <div class="col col-ppp-sec2">
                    <h1 class="flash animated infinite">On-Demand</h1>
                    <p>Kreatifmarket is a place where you can market your art works captured through the creativity of your mind or the lens of your camera. Your creativity is your extra cash. Why not earn them by joining us now. And yes, get other benefits
                        too!<br></p>
                </div>
            </div>
            <div class="row row-ppp2-space-mid">
                <div class="col col-ppp-sec2">
                    <ul>
                        <li>- Valid for one year</li>
                        <li>- Download at anytime</li>
                        <li>- No monthly commitment</li>
                    </ul>
                </div>
                <div class="col col-ppp-sec2">
                    <ul>
                        <li>- Yearly Subscription Plan</li>
                        <li>- Monthly Subscription Plan</li>
                    </ul>
                </div>
                <div class="col col-ppp-sec2">
                    <ul>
                        <li>- Yearly Subscription Plan</li>
                        <li>- Monthly Subscription Plan</li>
                    </ul>
                </div>
            </div>
            <div class="row row-ppp2-space-btm">
                <div class="col col-ppp-sec2">
                    <div class="card card-price" data-bs-hover-animate="pulse">
                        <div class="card-body">
                            <h4 class="card-title">Easy Pack Plan</h4>
                            <div class="row">
                                <div class="col col-price-title-left">
                                    <p>5 Downloads per year</p><span>RM 125.00</span></div>
                                <div class="col-xl-2 col-price-title-right"><input type="radio"></div>
                            </div>
                            <div class="row">
                                <div class="col col-price-title-left">
                                    <p>5 Downloads per year</p><span>RM 125.00</span></div>
                                <div class="col-xl-2 col-price-title-right"><input type="radio"></div>
                            </div>
                            <div class="row">
                                <div class="col col-price-title-left">
                                    <p>5 Downloads per year</p><span>RM 125.00</span></div>
                                <div class="col-xl-2 col-price-title-right"><input type="radio"></div>
                            </div>
                            <div class="row">
                                <div class="col col-price-title-left">
                                    <p>5 Downloads per year</p><span>RM 125.00</span></div>
                                <div class="col-xl-2 col-price-title-right"><input type="radio"></div>
                            </div>
                            <div class="row">
                                <div class="col"><button class="btn btn-primary" data-bs-hover-animate="pulse" type="button" style="margin-top: 151px;">Purchase</button></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col col-ppp-sec2">
                    <div class="card card-price" data-bs-hover-animate="pulse">
                        <div class="card-body">
                            <h4 class="card-title">1 Year Subscription Plan</h4>
                            <div class="row">
                                <div class="col col-price-title-left">
                                    <p>5 Downloads per year</p><span>RM 125.00</span></div>
                                <div class="col-xl-2 col-price-title-right"><input type="radio"></div>
                            </div>
                            <div class="row">
                                <div class="col col-price-title-left">
                                    <p>5 Downloads per year</p><span>RM 125.00</span></div>
                                <div class="col-xl-2 col-price-title-right"><input type="radio"></div>
                            </div>
                            <div class="row">
                                <div class="col col-price-title-left">
                                    <p>5 Downloads per year</p><span>RM 125.00</span></div>
                                <div class="col-xl-2 col-price-title-right"><input type="radio"></div>
                            </div>
                            <hr>
                            <h4 class="card-title">1 Month Subscription Plan</h4>
                            <div class="row">
                                <div class="col col-price-title-left">
                                    <p>5 Downloads per year</p><span>RM 125.00</span></div>
                                <div class="col-xl-2 col-price-title-right"><input type="radio"></div>
                            </div>
                            <div class="row">
                                <div class="col col-price-title-left">
                                    <p>5 Downloads per year</p><span>RM 125.00</span></div>
                                <div class="col-xl-2 col-price-title-right"><input type="radio"></div>
                            </div>
                            <div class="row">
                                <div class="col"><button class="btn btn-primary" data-bs-hover-animate="pulse" type="button">Purchase</button></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col col-ppp-sec2">
                    <div class="card card-price" data-bs-hover-animate="pulse">
                        <div class="card-body">
                            <h4 class="card-title">On-Demand Credits&nbsp;</h4>
                            <div class="row">
                                <div class="col col-price-title-left">
                                    <p>5 Downloads per year</p><span>RM 125.00</span></div>
                                <div class="col-xl-2 col-price-title-right"><input type="radio"></div>
                            </div>
                            <div class="row">
                                <div class="col col-price-title-left">
                                    <p>5 Downloads per year</p><span>RM 125.00</span></div>
                                <div class="col-xl-2 col-price-title-right"><input type="radio"></div>
                            </div>
                            <div class="row">
                                <div class="col col-price-title-left">
                                    <p>5 Downloads per year</p><span>RM 125.00</span></div>
                                <div class="col-xl-2 col-price-title-right"><input type="radio"></div>
                            </div>
                            <div class="row">
                                <div class="col col-price-title-left">
                                    <p>5 Downloads per year</p><span>RM 125.00</span></div>
                                <div class="col-xl-2 col-price-title-right"><input type="radio"></div>
                            </div>
                            <div class="row">
                                <div class="col"><button class="btn btn-primary" data-bs-hover-animate="pulse" type="button" style="margin-top: 151px;">Purchase</button></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section data-aos="fade-down" data-aos-duration="600" id="ppp-sec3">
        <div class="container">
            <div class="row row-ppp3-space">
                <div class="col col-ppp-sec3">
                    <h1 data-bs-hover-animate="flash">Start earning today</h1>
                    <p data-bs-hover-animate="flash">Contribute to Kreatifmarket and make money doing what you love.<br></p><button class="btn btn-primary" data-bs-hover-animate="pulse" type="button">Join Now</button>
                    <p>Already have an account?&nbsp;<a data-bs-hover-animate="flash" href="#">Sign in</a></p>
                </div>
                <div class="col"></div>
            </div>
        </div>
    </section>
    <section id="footer">
        @include('layouts.footer1')        
    </section>

    <section id="footer-merge">
        @include('layouts.footer1-merge')       
    </section>
    <script type="text/javascript" src="{{URL::asset('js/jquery.min.js')}}"></script>
    <script type="text/javascript" src="{{URL::asset('js/bootstrap.min.js')}}"></script>

  
    
    <script type="text/javascript" src="{{URL::asset('js/bs-animation.js')}}"></script>
    <script type="text/javascript" src="{{URL::asset('js/aos.js')}}"></script>
</body>

</html>