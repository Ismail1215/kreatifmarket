<!DOCTYPE html>
<html>

<head>
    <title>Forgot Password</title>
    @include('layouts.header-customer')
</head>

<body>
    <section id="hero">
        <div class="container">
            <div class="row rormidforgot"></div>
            <div class="row">
                <div class="col-xl-4"></div>
                <div class="col">
                    <div class="row">
                        <div class="col title-register">
                            <h2>Forgot your username or password?</h2>
                        </div>
                    </div>

                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    
                    <form method="POST" action="{{ route('password.update') }}">
                    
                             @csrf

                    <input type="hidden" name="token" value="{{ $token }}">

                    <div class="row">
                        <div class="col reg-input"> <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus readonly="">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                    </div>

                    <div class="row">
                        <div class="col reg-input"><input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password" placeholder="New Password ..">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                    </div>

                    <div class="row">
                        <div class="col reg-input"><input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password" placeholder="Re-Type New Password">
                            </div>
                    </div>
                    <div class="row req-field">
                        <div class="col">
                            <div class="info-req">
                                
                            </div>
                        </div>
                        <div class="col col-btn-reg"><button class="btn btn-primary" type="submit" style="background-color: rgb(168,5,93);border-style: none">Submit</button></div>
                    </div>
                    <div class="row">
                        <div class="col col-orlines">
                            <div class="orline"></div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col col-policy">
                            <div class="agree-policy"><span>Dont have an account yet?&nbsp;</span><a style="text-decoration: none;" href="{{ route('custom.register') }}">Sign Up</a><span>&nbsp; today!</span></div>
                        </div>
                        </form>
                    </div>
                </div>
                <div class="col-xl-4 offset-xl-0"></div>
            </div>
        </div>
    </section>
   
   <footer class="footer-mgn">
         @include('layouts.footer-customer')
    </footer>
    <script type="text/javascript" src="{{URL::asset('js/jquery.min.js')}}"></script>
    <script type="text/javascript" src="{{URL::asset('js/bootstrap.min.js')}}"></script>


</body>

</html>